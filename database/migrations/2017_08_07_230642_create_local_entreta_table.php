<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalEntretaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_entregas', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('id_empresa');
            $table->string('nome');
            $table->string('endereco');
            $table->integer('numero')->nullable()->default(0);
            $table->string('bairro')->nullable()->default('');
            $table->string('cidade')->nullable()->default('');
            $table->string('UF')->nullable()->default('');
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local_entregas');
    }
}
