<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('id_empresa');
            $table->string('razao');
            $table->string('fantasia');
            $table->smallInteger('is_pessoa_fisica');
            $table->string('cnpj',18);
            $table->string('cpf',14);
            $table->string('fone',16)->nullable()->default('');
            $table->string('email')->nullable()->default('');
            $table->string('ierg')->nullable()->default('');
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
