<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('OS', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('id_empresa');

            $table->integer('id_cliente');
//            $table->integer('id_osentrega');

            /* HORARIO */
            $table->timestamp('dtevento')->nullable();
            $table->timestamp('dtentrega')->nullable();
            $table->timestamp('dtrecolhimento')->nullable();

            /* ENTREGA */
            $table->integer('id_local_entrega')->nullable()->default(0);
            $table->string('entrega_nome')->nullable()->default('');
            $table->string('entrega_endereco')->nullable()->default('');
            $table->string('entrega_referencia')->nullable()->default('');
            $table->integer('entrega_numero')->nullable()->default(0);
            $table->string('entrega_bairro')->nullable()->default('');
            $table->string('entrega_cidade')->nullable()->default('');
            $table->string('entrega_UF')->nullable()->default('');

            $table->text('obscliente')->nullable();
            $table->text('obsinterna')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('OS');
    }
}
