<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('id_empresa');
            $table->integer('clientes_id')->unsigned()->nullable();
            $table->integer('fornecedores_id')->unsigned()->nullable();
            $table->string('nome');
            $table->string('cpf',14)->nullable()->default('');
            $table->string('rg')->nullable()->default('');
            $table->string('fone1')->nullable()->default('');
            $table->string('fone2')->nullable()->default('');
            $table->string('fone3')->nullable()->default('');
            $table->string('email')->nullable()->default('');
            $table->timestampsTz();

            $table->foreign('clientes_id')->references('id')->on('clientes');
            $table->foreign('fornecedores_id')->references('id')->on('fornecedores');
        });

}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contatos');
    }
}
