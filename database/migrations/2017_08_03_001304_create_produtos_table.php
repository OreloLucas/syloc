<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('id_empresa');
            $table->string('codigo')->nullable()->default('');
            $table->string('nome');
            $table->integer('fornecedores_id')->unsigned();
            $table->float('preco_loca')->nullable()->default('0');
            $table->float('preco_repo')->nullable()->default('0');
            $table->float('preco_custo')->nullable()->default('0');
            $table->text('descricao')->nullable();
            $table->text('imagem')->nullable();
            $table->timestampsTz();

            $table->foreign('fornecedores_id')->references('id')->on('fornecedores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
