<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOSitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('OSit', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('id_empresa');
            $table->integer('os_id');
            $table->integer('id_prod');
            $table->float('preco_loca')->nullable()->default('0');
            $table->integer('qtd');
            $table->timestamps();

            $table->foreign('os_id')->references('id')->on('OS');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('OSit');
    }
}
