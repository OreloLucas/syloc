<?php

use Illuminate\Database\Seeder;

class aboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('about')->delete();
        DB::table('empresas')->insert([
            'id' => '1',
            'razao' => 'Boutique de eventos',
            'fantasia' => 'Boutique de eventos',
            'cnpj' => '0',
            'fone' => 'Boutique de eventos',
            'email' => 'Boutique de eventos',
            'endereco' => 'Boutique de eventos',
            'numero' => 'Boutique de eventos',
            'complemento' => 'Boutique de eventos',
            'bairro' => 'Boutique de eventos',
            'municipio' => 'Boutique de eventos',
            'UF' => 'sc',
            'versao' => '1.1.0.0',
        ]);
//        DB::table('empresas')->insert([
//            'id' => '2',
//            'razao' => 'Boutique de eventos',
//            'fantasia' => 'Boutique de eventos',
//            'cnpj' => 'Boutique de eventos',
//            'fone' => 'Boutique de eventos',
//            'email' => 'Boutique de eventos',
//            'endereco' => 'Boutique de eventos',
//            'numero' => 'Boutique de eventos',
//            'complemento' => 'Boutique de eventos',
//            'bairro' => 'Boutique de eventos',
//            'municipio' => 'Boutique de eventos',
//            'UF' => 'sc',
//            'versao' => '1.1.0.0',
//        ]);
    }
}
