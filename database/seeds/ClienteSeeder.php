<?php

use Illuminate\Database\Seeder;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clientes = new \App\Clientes();
        $clientes->id = 0;
        $clientes->id_empresa=0;
        $clientes->razao='0';
        $clientes->fantasia='0';
        $clientes->cnpj='00.000.000/0000-00';
        $clientes->cpf='000.000.000-00';
        $clientes->is_pessoa_fisica = 0;
        $clientes->save();

        $clientes = new \App\Clientes();
        $clientes->id_empresa=1;
        $clientes->razao='teste 1';
        $clientes->fantasia='teste 1';
        $clientes->cnpj='00.000.000/0000-00';
        $clientes->cpf='000.000.000-00';
        $clientes->is_pessoa_fisica = 0;
        $clientes->save();

        $localentrega = new \App\LocalEntregas();
        $localentrega->id_empresa = 1;
        $localentrega->nome = 'casa';
        $localentrega->endereco= 'casa';
        $localentrega->numero= 1;
        $localentrega->cidade= 1;
        $localentrega->bairro= 'casa';
        $localentrega->UF= 'CA';
        $localentrega->save();

        $clientes = new \App\Fornecedores();
        $clientes->id = 0;
        $clientes->id_empresa=0;
        $clientes->razao='0';
        $clientes->fantasia='0';
        $clientes->cnpj='00.000.000/0000-00';
        $clientes->cpf='000.000.000-00';
        $clientes->is_pessoa_fisica = 0;
        $clientes->save();
    }
}
