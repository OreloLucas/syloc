@include('menu.layout')

<!doctype html>
<html lang="en">
<head>
    <title>Login - syloc </title>
</head>
<header>
    <img lass="img-responsive" src="{{asset('img/LOGO.jpg')}}" style="width: 25%;margin-left: 37.5%;">

</header>
</br>
<main>
    <div class="container" style="width: 50%; min-width: 50%">
        <form class="form-horizontal" method="post" action="{{route('login')}}">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Usuario  </label>
                <input  type="text" class="form-control" name="login" placeholder="Usuario" required>
                <label >Senha</label>
                <input type="password" class="form-control" name="password" placeholder="Senha" required>
                <br>
                <button type="submit" class="btn btn-default">Entrar</button>
                <div style="text-align: right">
                    <a href="{{ route('reset') }}" >Esqueceu a senha ?</a>
                </div>
            </div>

        </form>
        <div style="text-align: center">
            <br>
            @if(Session::has('mensagem'))
                <p class="{{ Session::get('mensagem')['class'] }}">{{ Session::get('mensagem')['msg'] }}</p>
            @endif
        </div>

    </div>
</main>
<footer>
    <small style="text-align: end;bottom: 5%;margin-left: 90%;position: fixed">
        <div style="text-align: right">
            teste ©®™
        </div>
    </small>
    @show
</footer>




