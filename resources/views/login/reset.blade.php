@include('menu.layout')

<!doctype html>
<html lang="en">
<head>

    <title>Reset - syloc </title>
</head>
<header>
<br>
</header>
<main>
    <div style="height: 30%"></div>
    <div class="container">
        <div class="container" style="width: 50%; min-width: 50%">
            @if($token === null || strlen($token) < 25)
            <form class="form-horizontal" method="post" action="{{route('reset')}}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label> digite seu email: </label>
                    <input  type="text" class="form-control" name="email" placeholder="email" required>
                    <button type="submit" class="btn btn-default">Enviar</button>
                    <div style="text-align: right">
                        <a href="{{ route('login') }}" >voltar</a>
                    </div>
                </div>
            </form>
            @else
                <form class="form-horizontal" method="post" action="{{route('login')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Nova senha  </label>
                        <input  type="password" class="form-control" id="password1" name="password1" placeholder="nova senha" onkeyup="valida()" required>
                        <label>Repita a senha</label>
                        <input type="password" class="form-control" id="password2" name="password2" placeholder="nova senha" onkeyup="valida()" required>
                        <br>
                        <button type="submit" id="submit" class="btn btn-default" disabled="disabled"
                                data-toggle="tooltip" data-placement="top" title="Tooltip on top">Trocar</button>
                        <div style="text-align: right">
                            <a href="{{ route('login') }}" >voltar</a>
                        </div>
                        <div id="submit_btn"></div>

                    </div>
                </form>
            @endif
            @if(Session::has('mensagem'))
                <p class="{{ Session::get('mensagem')['class'] }}">{{ Session::get('mensagem')['msg'] }}</p>
            @endif
        </div>
    </div>
</main>
<footer>
    @section('footer')

        <hr>
            <small style="text-align: end;bottom: 5%;margin-left: 90%;position: fixed">
                <div style="text-align: right">
                    teste ©®™
                </div>
            </small>
    @show
    @show
</footer>
