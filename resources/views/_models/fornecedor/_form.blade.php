{{ csrf_field() }}
<script type="text/javascript" src="{{ asset('js/model/Contato.js')}}"></script>
@if(isset($model))
    <div class="row form-group">
        <div class="col-xs-2">
            <label for="id">ID</label>
            <input type="text" name="id" id="id" class="form-control" placeholder="0"
                   @if(isset($model)) value="{{$model->id}}" @endif
                   readonly="readonly">
        </div>
    </div>
@endif

<div>
    <label>Pessoa Fisica</label>
    <input id="is_pessoa_fisica" name="is_pessoa_fisica" value="1" type="checkbox"
           onchange="isPessoaFisica(this)"
           onload="isPessoaFisica(this)"
           @if(View::getSections()['only_view'] == 'true') onclick="return false;" @endif
           @if(isset($model) && $model->is_pessoa_fisica == 1) checked
           @elseif(old('is_pessoa_fisica')==1) checked @endif>
</div>

<div class="row form-group">
    <div id="pj" class="col-xs-3">
        <label for="cnpj">CNPJ*</label>
        <input id="cnpj" type="text" name="cnpj" class="form-control" placeholder="__.___.___/____-__"
               @if(isset($model)) value="{{$model->cnpj}}" @else value="{{ old('cnpj') }}" @endif
               {{--onKeyPress="MascaraCNPJ(cnpj);"--}}
               data-mask="99.999.999/9999-99"
               maxlength="18"
        >
    </div>


    <div id="pf" class="col-xs-2">
        <label for="cpf">CPF</label>
        <input type="text" id="cpf" name="cpf" class="form-control" placeholder="___.___.___-__"
               @if(isset($model)) value="{{$model->cpf}}" @else value="{{ old('cpf') }}" @endif
               data-mask="999.999.999-99"
               maxlength=14
        >
    </div>

    <div class="col-xs-2">
        <label for="ierg">IE - RG</label>
        <input type="text" name="ierg" class="form-control" placeholder="IE"
               @if(isset($model)) value="{{$model->ierg}}" @else value="{{ old('ierg') }}" @endif
        >
    </div>

    @if((isset($model) && $model->is_pessoa_fisica == 1) || old('is_pessoa_fisica')==1)
        <script>
            document.getElementById("pj").style.visibility = 'hidden';
            document.getElementById("pf").style.visibility = 'visible';
        </script>
    @else
        <script>
            document.getElementById("pj").style.visibility = 'visible';
            document.getElementById("pf").style.visibility = 'hidden';
        </script>
    @endif
</div>

<div class="form-grouroup">
    <label for="razao">Razao*</label>
    <input type="text" name="razao" class="form-control" placeholder="Razao social"
           @if(isset($model)) value="{{$model->razao}}" @else value="{{ old('razao') }}" @endif
    >
</div>
<div class="form-group">
    <label for="fantasia">Fantasia*</label>
    <input type="text" name="fantasia" class="form-control" placeholder="Nome fantasia"
           @if(isset($model)) value="{{$model->fantasia}}" @else value="{{ old('fantasia') }}" @endif
    >
</div>


<div class="row form-group">
    <div id="pj" class="col-xs-3">
        <label for="fone">Telefone principal</label>
        <input id="fone" type="text" name="fone" class="form-control" placeholder="(___)_____-____"
               @if(isset($model)) value="{{$model->fone}}" @else value="{{ old('fone') }}" @endif
               data-mask="(999)9?9999-9999"
               maxlength="16"
        >
    </div>


    <div id="pf" class="col-xs-2">
        <label for="email">E-mail principal</label>
        <input type="text" id="email" name="email" class="form-control" placeholder="email@provedor.com"
               @if(isset($model)) value="{{$model->email}}" @else value="{{ old('email') }}" @endif
        >
    </div>
</div>


@if(isset($model))
    <div class="row form-group">
        <a class="btn btn-default" onclick="showAddContato()">Vincula Contato</a>
    </div>
    <div class="row form-group ">
        @include('_models.dates')
    </div>
@endif

@section('content2')
    @if(isset($model))
        <div class="container">
            <div class="row">
                <div class="col-md-11 ">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5> Contatos de {{ $model->fantasia  }}</h5>
                        </div>
                        <div class="panel-body">
                            <script>
                                $(document).ready(function () {
                                    $('#TblThisContatos').DataTable({
                                        "language": {
                                            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json",
                                        },
//                                        "info": false, "paging": false
                                    });
                                });
                            </script>

                            <table id="TblThisContatos" class="row-border  display compact">
                                <thead>
                                <tr>
                                    <th> nome</th>
                                    <th> email</th>
                                    <th> telefone</th>
                                    <th> telefone2</th>
                                    <th> telefone3</th>
                                    <th data-orderable="false" class="col-md-1">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($this_contatos as $c)
                                    <tr id="tr{{$c['id']}}">
                                        <td>{{ $c['nome'] }}</td>
                                        <td>{{ $c['email'] }}</td>
                                        <td>{{ $c['fone1'] }}</td>
                                        <td>{{ $c['fone2'] }}</td>
                                        <td>{{ $c['fone3'] }}</td>
                                        <td>
                                            <a id="desvincula"
                                               onclick="desvinculaContatoF({{ $c['id'] }})"
                                               class="btn btn-danger btn-xs glyphicon glyphicon-trash">
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <br>
                            {{--<a type=submit id="openContatosModal" class="btn btn-success col-md-offset-0"--}}
                            {{--data-toggle="modal"--}}
                            {{--data-target="#myModal"--}}
                            {{--@if(View::getSections()['only_view'] == 'true') style="display: none;" @endif>--}}
                            {{--Vincular contato</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

<!-- Modal -->
{{--@if(View::getSections()['only_view'] == 'false')--}}
{{--<div class="container">--}}
{{--<div class="modal fade" id="myModal" role="dialog">--}}
{{--<div class="modal-dialog">--}}

{{--<!-- Modal content-->--}}
{{--<div class="modal-content">--}}
{{--<div class="modal-header">--}}
{{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--<h4 class="modal-title">Modal Header</h4>--}}
{{--</div>--}}
{{--<div class="modal-body">--}}
{{----}}
{{--<script>--}}
{{--$(document).ready(function () {--}}
{{--$('#TblAllContatos').DataTable({--}}
{{--"language": {--}}
{{--"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"--}}
{{--},--}}
{{--//                                            "info": false--}}
{{--});--}}
{{--});--}}
{{--</script>--}}
{{--<table id="TblAllContatos" class="row-border  display compact">--}}
{{--<thead>--}}
{{--<tr>--}}
{{--<th> nome</th>--}}
{{--<th> email</th>--}}
{{--<th> telefone</th>--}}
{{--<th> telefone1</th>--}}
{{--<th> ação</th>--}}
{{--</tr>--}}
{{--</thead>--}}

{{--<tbody>--}}
{{--@foreach($contatos as $c)--}}
{{--<tr id="Alltr{{$c['id']}}">--}}
{{--<td>{{ $c['nome'] }}</td>--}}
{{--<td>{{ $c['email'] }}</td>--}}
{{--<td>{{ $c['fone1'] }}</td>--}}
{{--<td>{{ $c['fone2'] }}</td>--}}
{{--<td>--}}
{{--<a id="desvincula" name="desvincula{{$c['id']}}"--}}
{{--data-cliente="{{ $model['id'] }}"--}}
{{--data-contato="{{ $c['id'] }}"--}}
{{--class="btn btn-danger btn-xs glyphicon glyphicon-trash"--}}
{{--style="visibility: hidden">--}}
{{--</a>--}}
{{--<a id="vincula" name="vincula{{$c['id']}}"--}}
{{--data-cliente="{{ $model['id'] }}"--}}
{{--data-contato="{{ $c['id'] }}"--}}
{{--class="btn btn-success btn-xs glyphicon glyphicon-ok-circle"--}}
{{--style="visibility: hidden">--}}
{{--</a>--}}

{{--<script>--}}
{{--var refClientes_id = '{{$c['clientes_id']}}';--}}
{{--var cliente_id = '{{$model['id']}}';--}}
{{--var idContato = '{{$c['id']}}';--}}

{{--if (refClientes_id === cliente_id) {--}}
{{--document.getElementsByName("desvincula" + idContato).item(0).style.visibility = 'visible';--}}
{{--document.getElementsByName("vincula" + idContato).item(0).style.visibility = 'hidden';--}}

{{--} else {--}}
{{--document.getElementsByName("desvincula" + idContato).item(0).style.visibility = 'hidden';--}}
{{--document.getElementsByName("vincula" + idContato).item(0).style.visibility = 'visible';--}}
{{--}--}}
{{--</script>--}}
{{--</td>--}}
{{--</tr>--}}
{{--@endforeach--}}
{{--</tbody>--}}
{{--</table>--}}
{{--</div>--}}
{{--<div class="modal-footer">--}}
{{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--@endif--}}
{{--@endif--}}
{{--@endsection--}}


