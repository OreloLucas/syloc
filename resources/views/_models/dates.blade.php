

    <div class="col-xs-4">
        <label for="cnpj">Data Cadastro</label>
        <input type="text" name="created_at" class="form-control" placeholder="0"
               value="{{  date('d/m/Y H:i', strtotime($model->created_at))  }}"
               readonly="readonly" minlength=18>
    </div>
    <div class="col-xs-4">
        <label for="ie">Ultima atualização</label>
        <input type="datetime" name="updated_at" class="form-control" placeholder="0"
               value="{{ date('d/m/Y H:i', strtotime($model->updated_at))  }}"
               readonly="readonly">

    </div>
