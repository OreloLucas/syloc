{{ csrf_field() }}
<input type="hidden" id="clientes_id" name="clientes_id" class="form-control" value="0">
<input type="hidden" id="clientes_id" name="fornecedores_id" class="form-control" value="0">

@if(isset($model))
    <div class="row form-group">
        <div class="col-xs-2">
            <label for="id">ID</label>
            <input type="text" name="id" class="form-control" placeholder="0"
                   @if(isset($model)) value="{{$model->id}}" @endif
                   readonly="readonly">
        </div>
    </div>
@endif

<div class="row form-group">
    <input type="hidden" id="id" value="0">
    <div class="col-xs-12">
        <label> Nome</label>
        <input type="text" name="nome" id="nome" class="form-control" placeholder="Nome"
               @if(isset($model)) value="{{$model->nome }}" @else value="{{ old('nome') }}" @endif >
    </div>
</div>
<div class="row form-group">
    <div class="col-xs-6">
        <label> RG</label>
        <input type="text" name="rg" id="rg" class="form-control" placeholder="RG"
               @if(isset($model)) value="{{$model->rg }}" @else value="{{ old('rg') }}" @endif>
    </div>
    <div class="col-xs-6">
        <label> CPF</label>
        <input type="text" name="cpf" id="cpf" class="form-control"
               placeholder="___.___.___-__"
               data-mask="999.999.999-99"
               @if(isset($model)) value="{{$model->cpf }}" @else value="{{ old('cpf') }}" @endif>
    </div>
</div>
<script>
    function t (){
        document.getElementById("fone1").setAttribute("data-mask", "(999) 9999-9999");
        console.log('aaa');
    }
</script>


<div class="row form-group">
    <div class="col-xs-6">
        <label> E - mail</label>
        <input type="text" name="email" id="email" class="form-control" placeholder="E-mail"
               @if(isset($model)) value="{{$model->email }}" @else value="{{ old('email') }}" @endif>
    </div>
    <div class="col-xs-6">
        <label> Telefone</label>
        <input type="text" name="fone1" id="fone1" class="form-control"
               onblur="t()"
               data-mask="(999)9 9999-9999"
               data-placeholder="-"
               pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}"
               placeholder="(___)_____-____"
               @if(isset($model)) value="{{$model->fone1 }}" @else value="{{ old('fone1') }}" @endif>
    </div>
</div>


<div class="row form-group">
    <div class="col-xs-6">
        <label> Telefone 2 </label>
        <input type="text" name="fone2" id="fone2" class="form-control" data-mask="(999)9?9999-9999"
               placeholder="(___)_____-____"
               @if(isset($model)) value="{{$model->fone2 }}" @else value="{{ old('fone2') }}" @endif>
    </div>
    <div class="col-xs-6">
        <label> Telefone 3 </label>
        <input type="text" name="fone3" id="fone3" class="form-control" data-mask="(999)*? 9999-9999"
               placeholder="(___)_____-____"
               @if(isset($model)) value="{{$model->fone3 }}" @else value="{{ old('fone3') }}" @endif>
    </div>
</div>
@if(isset($model))
    <div class="row form-group ">
        @include('_models.dates')
    </div>
@endif