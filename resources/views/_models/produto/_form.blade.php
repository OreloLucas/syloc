{{ csrf_field() }}
<input type="hidden" id="fornecedores_id" name="fornecedores_id" class="form-control" value="0">

@if(isset($model))
    <div class="row form-group">
        <div class="col-xs-2">
            <label for="id">ID</label>
            <input type="text" name="id" class="form-control" placeholder="0"
                   @if(isset($model)) value="{{$model->id}}" @endif
                   readonly="readonly">
        </div>
    </div>
@endif

<div class="row form-group">
    <div class="col-xs-3">
        <label for="codigo">codigo </label>
        <input type="text" id="codigo" name="codigo" class="form-control" placeholder=""
               @if(isset($model)) value="{{$model->codigo }}" @else value="{{ old('codigo') }}" @endif >
    </div>

    <div class="col-xs-2">
        <label for="nome">nome</label>
        <input type="text" id="nome" name="nome" class="form-control" placeholder=""
               @if(isset($model)) value="{{$model->nome }}" @else value="{{ old('nome') }}" @endif>
    </div>
</div>

<div class="row form-group">
    <div class="col-xs-3">
        <label for="preco_loca">preco locação</label>
        <div class="input-group">
            <div class="input-group-addon">$</div>
            <input type="number" step="any" name="preco_loca" class="form-control"
                   @if(isset($model)) value="{{$model->preco_loca }}" @else value="{{ old('preco_loca') }}" @endif
                   onblur="formatDecimal(this)">
        </div>

    </div>

    <div class="col-xs-3">
        <label for="preco_repo">preco reposição</label>
        <div class="input-group">
            <div class="input-group-addon">$</div>
            <input type="number" step="any" name="preco_repo" class="form-control"
                   @if(isset($model)) value="{{$model->preco_repo }}" @else value="{{ old('preco_repo') }}" @endif
                   onblur="formatDecimal(this)">
        </div>

    </div>

    <div class="col-xs-3">
        <label for="preco_custo">preco custo</label>
        <div class="input-group">
            <div class="input-group-addon">$</div>
            <input type="number" step="any" name="preco_custo" class="form-control" id="preco_custo"
                   @if(isset($model)) value="{{$model->preco_custo }}" @else value="{{ old('preco_custo') }}" @endif
                   onblur="formatDecimal(this)">
        </div>

    </div>

</div>

@if(isset($model))
    <div class="row form-group">
        <div class="col-xs-12">
            <img src="{{ asset('storage/'.$model->imagem ) }}" width="380" height="250"/>
        </div>
    </div>
@endif

@if(View::getSections()['only_view'] == 'false')
    <div class="fileinput fileinput-new input-group col-xs-7" data-provides="fileinput">
        <div class="form-control" data-trigger="fileinput">
            <i class="glyphicon glyphicon-file fileinput-exists"></i>
            <span class="fileinput-filename"></span>
        </div>
        <span class="input-group-addon btn btn-default btn-file">
            <span class="fileinput-new">Selecione arquivo</span>
            <span class="fileinput-exists">Change</span>
                <input type="file" name="imagem">
            </span>
        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
    </div>
@endif

<div class="row form-group">
    <div class="col-xs-3">
        <label for="codigo">descrição </label>
        <textarea class="field" rows="4" cols="50" id="descricao" name="descricao">@if(isset($model)){{$model->descricao }}@else{{ old('descricao')}}@endif</textarea>
    </div>
</div>

@if(isset($model))
    <div class="row form-group ">
        @include('_models.dates')
    </div>
@endif

