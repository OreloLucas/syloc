{{ csrf_field() }}
@if(isset($model))
    <div class="row form-group" >
        <div class="col-xs-2">
            <label for="id">ID</label>
            <input type="text" name="id" class="form-control" placeholder="0"
                   @if(isset($model)) value="{{$model->id}}" @endif
                   readonly="readonly">
        </div>
    </div>
@endif

<div class="row form-group">
    <div class="col-xs-6">
        <label for="nome">Nome :</label>
        <input type="text" id="nome" name="nome" class="form-control" placeholder=""
               @if(isset($model)) value="{{$model->nome }}" @else value="{{ old('nome') }}" @endif >
    </div>

    {{--<div class="col-xs-2">--}}
        {{--<label for="nome">nome</label>--}}
        {{--<input type="text" id="nome" name="nome" class="form-control" placeholder=""--}}
               {{--@if(isset($model)) value="{{$model->nome }}" @endif>--}}
    {{--</div>--}}
</div>

<div class="row form-group">
    <div class="col-xs-10">
        <label for="endereco">Endereco :</label>
        <input type="text" name="endereco" class="form-control" placeholder=""
               @if(isset($model)) value="{{$model->endereco }}" @else value="{{ old('endereco') }}" @endif >
    </div>

    <div class="col-xs-2">
        <label>Numero :</label>
        <input type="text" name="numero" class="form-control" placeholder=""
               @if(isset($model)) value="{{$model->numero }}" @else value="{{ old('numero') }}" @endif>
    </div>
</div>


<div class="row form-group">

    <div class="col-xs-5">
        <label>Bairro :</label>
        <input type="text" name="bairro" class="form-control" placeholder=""
               @if(isset($model)) value="{{$model->bairro }}" @else value="{{ old('bairro') }}" @endif >
    </div>

    <div class="col-xs-5">
        <label for="cidade">Cidade :</label>
        <input type="text" name="cidade" class="form-control" placeholder=""
               @if(isset($model)) value="{{$model->cidade }}" @else value="{{ old('cidade') }}" @endif >
    </div>

    <div class="col-xs-2">
        <label>UF :</label>
        <input type="text" name="UF" class="form-control" placeholder=""
               style="text-transform:uppercase"
               @if(isset($model)) value="{{$model->UF }}" @else value="{{ old('UF') }}" @endif
                maxlength="2">
    </div>
</div>

@if(isset($model))
    <div class="row form-group ">
        @include('_models.dates')
    </div>
@endif

