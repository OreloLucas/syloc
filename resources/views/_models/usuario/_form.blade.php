{{ csrf_field() }}
@if(isset($model))
    <div class="row form-group" >
        <div class="col-xs-2">
            <label for="id">ID</label>
            <input type="text" name="id" class="form-control" placeholder="0"
                   value="{{$model->id}}"
                   readonly="readonly">
        </div>
    </div>
@endif

<div class="row form-group">
    <div class="col-xs-3">
        <label for="name">Nome*</label>
        <input id="name" type="text" name="name" class="form-control" placeholder=""
               @if(isset($model)) value="{{$model->name}}" @else value="{{ old('name') }}"@endif >
    </div>


    <div class="col-xs-3">
        <label for="sobrenome">Sobrenome</label>
        <input type="text" id="sobrenome" name="sobrenome" class="form-control" placeholder=""
               @if(isset($model)) value="{{$model->sobrenome}}" @else value="{{ old('sobrenome') }}" @endif >
    </div>
</div>

<div class="row form-group">
    <div class="col-xs-3">
        <label for="email">email*</label>
        <input id="email" type="email" name="email" class="form-control" placeholder=""
               @if(isset($model)) value="{{$model->email}}" @else value="{{ old('email') }}"@endif >
    </div>
</div>

@if(!isset($model))
    <div class="row form-group">
        <div class="col-xs-3">
            <label for="password1">Senha*</label>
            <input type="password" id="password1" name="password1" class="form-control" placeholder="digite a senha"
                   >
        </div>


        <div class="col-xs-3">
            <label for="password2">Repita a senha</label>
            <input type="password" id="password2" name="password2" class="form-control" placeholder="redigite a senha">
        </div>
    </div>
@endif
{{--BOTAO RECUPERA SENHA--}}
{{--@if(!(View::getSections()['only_view'] == 'true'))--}}
{{--<div class="row form-group">--}}
{{--<div id="pj" class="col-xs-3">--}}
{{--<label for="password1">senha</label>--}}
{{--<input type="password" name="password1" class="form-control" placeholder="preencha para editar a senha" >--}}
{{--</div>--}}


{{--<div id="pf" class="col-xs-3">--}}
{{--<label for="sobrenome">repita a senha</label>--}}
{{--<input type="password" name="password2" class="form-control" placeholder="redigite a senha" >--}}
{{--</div>--}}
{{--</div>--}}
{{--@endif--}}

@if(isset($model))
    <div class="row form-group ">
        @include('_models.dates')
    </div>
@endif

