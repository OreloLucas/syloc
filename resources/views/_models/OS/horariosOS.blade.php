{{--DATAS --}}


<div class="row form-group">
    {{--EVENTO--}}
    <div class="col-xs-2">
        <label>Data Evento : </label>
        <input type="text" id="dtevento" name="dtEvento" class="form-control"
               @if(isset($model))value="{{$model->dtevento }}"@else{{ old('dtevento')}}@endif data-mask="99/99/9999">
    </div>
    <div class="col-xs-2">
        <label>Hora Evento : </label>
        <input type="text" id="horaEvento" name="horaEvento" class="form-control"
               @if(isset($model))value="{{$model->horaEvento }}" @else{{ old('horaEvento')}}@endif
               onblur="ajustaHora(this)" data-mask="99:99">
    </div>

    {{--ENTREGA--}}
    <div class="col-xs-2">
        <label>Data Entrega : </label>
        <input type="text" id="dtentrega" name="dtEntrega" class="form-control"
               @if(isset($model))value="{{$model->dtentrega }}"@else{{ old('dtentrega')}}@endif data-mask="99/99/9999">
    </div>

    <div class="col-xs-2">
        <label>Hora Entrega : </label>
        <input type="text" id="horaEntrega" name="horaEntrega" class="form-control"
               @if(isset($model)) value="{{$model->horaEntrega }}" @else{{ old('horaEntrega')}}@endif
               onblur="ajustaHora(this)" data-mask="99:99">
    </div>

    {{--RECOLHIMENTO--}}
    <div class="col-xs-2">
        <label>Data Recolhimento : </label>
        <input type="text" id="dtrecolhimento" name="dtRecolhimento" class="form-control"
               @if(isset($model))value="{{$model->dtrecolhimento }}"@else{{ old('dtrecolhimento')}}@endif data-mask="99/99/9999">
    </div>

    <div class="col-xs-2">
        <label>Hora Recolhimento : </label>
        <input type="text" id="horaRecolhimento" name="horaRecolhimento" class="form-control"
               @if(isset($model))value="{{$model->horaRecolhimento }}"@else{{ old('horaRecolhimento')}}@endif
               onblur="ajustaHora(this)" data-mask="99:99">
    </div>
</div>
