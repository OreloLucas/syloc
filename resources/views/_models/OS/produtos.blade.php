{{--@extends('menu.header')--}}

{{--@section('titulo',$formName)--}}
{{--{{dd($this_produto)}}--}}




@if(isset($model))

    <div class="container">
        <div class="row"></div>
        <div class="col-md-11 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5> Produtos da OS </h5>
                </div>
                <div class="panel-body">
                    {{--<div class="row form-group ">--}}
                    {{--<div id="pj" class="col-xs-2">--}}
                    {{--<a class="btn btn-default" onclick="showAddContato()">Vincula Contato</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--<div>--}}
                    {{--<label>Produto</label>--}}
                    {{--<div class="row form-group">--}}
                    {{--<div class="col-xs-10">--}}
                    {{--<input id="busca_produto" type="text" class="form-control">--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-2">--}}
                    {{--<a class="btn btn-default" onclick="buscaProduto(busca_produto.value)">Buscar produto</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <script>
                        $(document).ready(function () {
                            $('#TblThisProdutos').DataTable({
                                "language": {
                                    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json",
                                }
                            });
                        });
                        $(document).ready(function () {
                            $('#TblBuscaProdutos').DataTable({
                                "language": {
                                    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json",
                                }
                            });
                        });
                    </script>
                    <table id="TblBuscaProdutos" class="row-border  display compact">
                        <thead>
                        <tr>
                            <th> Cod</th>
                            <th> Nome</th>
                            <th> Locação</th>
                            <th> Reposição</th>
                            <th> Custo</th>
                            <th data-orderable="false" class="col-md-1">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($busca_produto as $bp)
                            <tr id="tr{{$bp['id']}}">
                                <td>{{ $bp['codigo'] }}</td>
                                <td>{{ $bp['nome'] }}</td>
                                <td>{{ $bp['preco_loca'] }}</td>
                                <td>{{ $bp['preco_repo'] }}</td>
                                <td>{{ $bp['preco_custo'] }}</td>
                                <td>
                                    <a id="addToOS"
                                       data-cliente="{{ $model['id'] }}"
                                       data-contato="{{ $bp['id'] }}"
                                       {{--onclick="addToOS( {{$bp['id']}} , {{$model['id']}} )"--}}
                                       onclick="loadAddProduto(); showAddProduto({{$bp['id']}} , {{$model['id']}}, {{ $bp['preco_loca'] }})"
                                       {{--btn altera dados--}}
                                       class="btn btn-info btn-xs glyphicon glyphicon-plus"></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{--GRID DE CONSULTA--}}
                    {{--GRID COM ITENS DO PEDIDO--}}

                    <table id="TblThisProdutos" class="row-border  display compact">
                        <thead>
                        <tr>
                            <th> Cod</th>
                            <th> Nome</th>
                            <th> Locação</th>
                            <th> Quantidade</th>
                            <th> Reposição</th>
                            <th> Custo</th>
                            <th data-orderable="false" class="col-md-1">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{--ROTINA PARA ALTERAR ITEM JÁ INCLUIDO NA OS--}}
                        {{--não listar equipamentos que estão incluidos na OS--}}
                        @foreach($this_produto as $tp)
                            <tr id="tr{{$tp['id']}}">
                                <td>{{ $tp['codigo'] }}</td>
                                <td>{{ $tp['nome'] }}</td>
                                <td>{{ number_format((float)$tp['preco_loca'], 2, '.', '')  }}</td>
                                <td>{{ $tp['qtd'] }}</td>
                                <td>{{ $tp['preco_repo'] }}</td>
                                <td>{{ $tp['preco_custo'] }}</td>
                                <td>
                                    <a id="addToOS"
                                       data-cliente="{{ $model['id'] }}"
                                       data-contato="{{ $tp['id'] }}"
                                       onclick="deleteToOS( {{$tp['id']}} )"
                                       class="btn btn-danger btn-xs glyphicon glyphicon-minus"></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
@endif

<script>
    $(document).ready(function () {
        loadAddProduto();
    });
</script>
