<div class="row form-group">
    <div class="col-xs-6">
        <label for="codigo">Descrição Cliente</label>
        <textarea class="field col-xs-12 form-control" rows="4" cols="50" id="obscliente" name="obscliente"
                  >@if(isset($model)){{$model->obscliente }}@else{{ old('obscliente')}}@endif</textarea>
    </div>
    <div class="col-xs-6">
        <label for="codigo">Descrição Interna </label>
        <textarea class="field col-xs-12 form-control" rows="4" cols="50" id="obsinterna" name="obsinterna"
                >@if(isset($model)){{$model->obsinterna }}@else{{ old('obsinterna')}}@endif</textarea>
    </div>
</div>
