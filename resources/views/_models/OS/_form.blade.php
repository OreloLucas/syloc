{{ csrf_field() }}
@if(isset($model))
    <div class="row form-group">
        <div class="col-xs-2">
            <label for="id">ID</label>
            <input type="text" name="id" class="form-control" placeholder="0"
                   value="{{$model->id}}"
                   readonly="readonly">
        </div>
    </div>
@endif

<div>


    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Clientes</a></li>
        <li><a data-toggle="tab" href="#menu1">Local Entrega</a></li>
        <li><a data-toggle="tab" href="#menu2">Horários</a></li>
        <li><a data-toggle="tab" href="#menu3">Observações</a></li>
    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <br>
            @include('_models.OS.clienteOS')
        </div>
        <div id="menu1" class="tab-pane fade">
            <br>
            @include('_models.OS.localEntregaOS')
        </div>
        <div id="menu2" class="tab-pane fade">
            <br>
            @include('_models.OS.horariosOS')
        </div>
        <div id="menu3" class="tab-pane fade">
            <br>
            @include('_models.OS.observacaoOS')
        </div>
    </div>
</div>




{{--adiciona produtos--}}

@if(isset($model))
    <div class="row form-group ">
        @include('_models.dates')
    </div>
@endif

@section('content2')

@include('_models.OS.produtos')
@endsection