<style>
    table {
        border: 1px solid black;
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td,th {
        border: 1px solid black;
        vertical-align: middle;
        text-align: center;
        font-size: 12px;
        white-space: nowrap;
        overflow: hidden;
    }

    td a, td input, td img {
        border: 1px solid black;
        vertical-align: middle;
        display: inline-block;
    }
</style>

{{--CABEÇALHO--}}

<table>
    <tr>
        <td rowspan="5"><img src="{{ public_path() . '/img/logo.jpg' }}" style="height:50px; width:50px;border:0;"></td>
        <td>{{ $empresa->razao }}</td>
       <td></td>
    </tr>
    <tr>
        <td>{{ $empresa->endereco . ', N '. $empresa->numero . ' - '.$empresa->complemento }}</td>
        <td> Ordem Serviço : {{ $os->id }}</td>
    </tr>
    <tr>
        <td>{{ $empresa->bairro . ' - '. $empresa->municipio . '/'.$empresa->UF }}</td>
        <td></td>
    </tr>
    <tr>
        <td>FONE : {{ $empresa->fone}}</td>
        <td></td>
    </tr>
    <tr>
        <td> {{ $empresa->email}}</td>
        <td> 11/10/2018 </td>
    </tr>
</table>

<br>

<table>
    <tr>
        <td colspan="2"> Presado Cliente </td>
    </tr>
    <tr>
        <td> Cliente : {{ $cliente->fantasia }} </td>
        <td> empresa : {{ $cliente->razao }} </td>
    </tr>
    <tr>
        <td> fone : {{ $cliente->fone }} </td>
        <td> email : {{ $cliente->email }} </td>
    </tr>
    <tr>
        <td colspan="2"> endereço : {{ $entrega->endereco }} </td>
    </tr>

</table>


{{--CABEÇALHO--}}
{{--CLIENTE--}}

{{--ENDEREÇO ENTREGA--}}


{{--ITENS--}}


{{--FORMA DE PAGAMENTO: À VISTA--}}
{{--TIPO DE PAGAMENTO: DEPÓSITO BANCÁRIO--}}
{{--DATA DO EVENTO: 03/01/2013--}}
{{--DATA DE ENTREGA: 03/01/2013 00:00 DATA DE RETIRADA: 04/01/2013 00:00--}}
{{--LOCAL DA ENTREGA: ESPAÇO ONDAS - INGLESES--}}
{{--RESP. RECEBIMENTO: SR. MARCUS--}}

{{---- ASSINATURAS ----}}


{{--OBSERVAÇÕES--}}


{{--SERÁ COBRADO REPOSIÇÃO QUANDO:--}}
{{--- O MATERIAL LOCADO FOR DEVOLVIDO: QUEBRADO, RASGADO, QUEIMADO, RISCADO, MANCHADO DE VELAS, CANETAS, PULSEIRA DE NEON, CONFETES E SERPENTINAS OU EXTRAVIADOS--}}
{{--- VIDROS QUEBRADOS OU LASCADOS;--}}

{{--TERMO DE RESPONSABILIDADE--}}
{{--2A PRODUÇÕES -MARCUS NILZO DA SILVA, COM ENDEREÇO À RUA RUA SOUZA DUTRA, 633 SALA 1, BAIRRO ESTREITO, CIDADE DE FLORIANÓPOLIS, INSCRITA NO CNPJ/CPF Nº 12855030000169, DORAVANTE DENOMINADO(A) CONTRATANTE, ME RESPONSABILIZO POR EVENTUAIS DANOS OU EXTRAVIOS CAUSADOS NAS PEÇAS LOCADAS, CONFORME DESCRITO NESTA ORDENS DE SERVIÇO, COM O CONSERTO OU A REPOSIÇÃO DAS MESMAS.--}}

{{--FLORIANÓPOLIS, 04 DE JANEIRO DE 2013--}}

{{--________________________________________--}}
{{--2A PRODUÇÕES -MARCUS NILZO DA SILVA - 12855030000169--}}
{{--CONTRATANTE--}}
{{--ASSINATURA NA DEVOLUÇÃO DAS PEÇAS--}}