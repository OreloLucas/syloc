{{--busca local de entrega--}}
<div>
    <div class="row form-group">
        <div class="col-xs-8">
            <label for="buscalocalentrega"> Busca Local de entrega</label>
            <input type="text" id="buscalocal" class="form-control"
                   onkeyup="buscaLocalEntrega(this.value)">
        </div>

    </div>
</div>

<hr style="
                  display: block;
                  height: 1px;
                  border: 0;
                  border-top: 1px solid #ccc;
                  margin: 1em 0;
                  padding: 0;">

<div class="row form-group">
    <div class="col-xs-12">
        <label for="codigo">Locan Entrega</label>
        <textarea class="field col-xs-12 form-control" rows="4" cols="50" id="localentrega" name="localentrega"
                  disabled="true"
                  style="font-size: 16px;">@if(isset($model)){{$model->entrega}} @else
                "{{ old('entrega') }} @endif</textarea>
    </div>
</div>
{{--dados local entrega--}}
<input id="id_local_entrega" name="id_local_entrega" type="hidden"
       @if(isset($model)) value="{{$model->id_local_entrega}}" @else value="{{ old('id_local_entrega') }}" @endif>
