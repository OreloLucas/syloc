
<div>
    {{--busca cliente--}}
    @if(!isset($model))
        <div>
            <div class="row form-group">
                <div class="col-xs-10">
                    <label> Busca Cliente </label>
                    <input type="text" id="buscacliente" class="form-control"
                           onkeyup="buscaCliente(this.value)">
                </div>
            </div>
        </div>
        <hr style="
                  display: block;
                  height: 1px;
                  border: 0;
                  border-top: 1px solid #ccc;
                  margin: 1em 0;
                  padding: 0;">

    @endif
    {{--dados cliente--}}
    <div>
        <input id="id_cliente" name="id_cliente" type="hidden"  @if(isset($model)) value="{{$model->id_cliente}}" @else value="0" @endif>
        <div class="row form-group">
            <div class="col-xs-5">
                <label>Nome Cliente</label>
                <input id="cliente" type="text" class="form-control" disabled="true"
                       @if(isset($model)) value="{{$model->razao}}" @endif >
            </div>
            <div class="col-xs-5">
                <label> CNPJ </label>
                <input id="cnpj" type="text" class="form-control" disabled="true"
                       @if(isset($model)) value="{{$model->cnpj}}" @endif>
            </div>
        </div>
    </div>

</div>