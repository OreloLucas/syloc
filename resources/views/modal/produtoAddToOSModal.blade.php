{{--Created by PhpStorm.--}}
{{--User: Lucas--}}
{{--Date: 06/11/2017--}}
{{--Time: 00:28--}}

{{--<script type="text/javascript" src="{{ asset('js/model/Contato.js')}}"></script>--}}
<div class="modal fade" id="produtoAddToOSModal" role="dialog">
    <div class="modal-dialog" role="dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> Contato</h4>
            </div>
            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-xs-12">
                        <label>Quantidade :</label>
                        <input class="form-control" type="number" id="modal_qtd" placeholder="" value="1">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-xs-12">

                        <label>Valor Locação :</label>
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="number" step="any" name="modal_preco_loca" class="form-control"
                                  id="modal_preco_loca"
                                   onblur="formatDecimal(this)">
                        </div>

                        {{--<input type="number" name="nome" id="modal_preco_loca" class="form-control" placeholder="" value="">--}}
                    </div>
                </div>
                {{--<hr style="--}}
                  {{--display: block;--}}
                  {{--height: 1px;--}}
                  {{--border: 0;--}}
                  {{--border-top: 1px solid #ccc;--}}
                  {{--margin: 1em 0;--}}
                  {{--padding: 0;">--}}
                {{--<div class="row form-group">--}}
                    {{--<input type="hidden" id="idContato" value="0">--}}
                    {{--<div class="col-xs-12">--}}
                        {{--<label> Nome</label>--}}
                        {{--<input type="text" name="nome" id="nomeContato" class="form-control" placeholder="Nome">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row form-group">--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<label> RG</label>--}}
                        {{--<input type="text" name="RG" id="rgContato" class="form-control" placeholder="RG">--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<label> CPF</label>--}}
                        {{--<input type="text" name="CPF" id="cpfContato" class="form-control"--}}
                               {{--placeholder="___.___.___-__"--}}
                               {{--data-mask="999.999.999-99">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row form-group">--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<label> E - mail</label>--}}
                        {{--<input type="text" name="E-mail1" id="emailContato" class="form-control" placeholder="E-mail">--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<label> Telefone</label>--}}
                        {{--<input type="text" name="Fone1" id="fone1Contato" class="form-control"--}}
                               {{--data-mask="(999)9?9999-9999"--}}
                               {{--pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}"--}}
                               {{--placeholder="(___)_____-____">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row form-group">--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<label> Telefone 2 </label>--}}
                        {{--<input type="text" name="Fone2" id="fone2Contato" class="form-control" data-mask="(999)9?9999-9999"--}}
                               {{--placeholder="(___)_____-____">--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<label> Telefone 3 </label>--}}
                        {{--<input type="text" name="Fone3" id="fone3Contato" class="form-control" data-mask="(999)9?9999-9999"--}}
                               {{--placeholder="(___)_____-____">--}}
                    {{--</div>--}}
                {{--</div>--}}
                <a class="btn btn-success" id="vinculaContato" onclick="addToOS()">Vincula</a>
            </div>
        </div>
    </div>
</div>