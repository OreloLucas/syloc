 @include('menu.layout')

<!doctype html>
<html lang="en">
<head>
    <title>@yield('titulo') - syloc </title>
</head>
<body class="content"
      @if(isset(View::getSections()['only_view']) && View::getSections()['only_view'] == 'true')
      onload="disable_if_readonly()"
      @endif >
<header>
    @section('header')
        <div class="row">
            <div class="col-sm-12" style="height:15%">
            <div  class="largeHeader">
                <a href="{{ route('dashboard') }}">
                <img class="img-responsive" src="{{ asset('img/LOGO.jpg') }}" alt="logo" style="height:100%; width:15%;border:0;">
                </a>
                <p style="width:auto;position:absolute;top: 40%;margin-left: 16%">
                    Bem vindo {{ auth()->user()['name'] }}
                </p>
            </div>
                <div class="smallHeader">
                    <p style="width:auto;position:absolute;top: 40%;margin-left: 5%">
                        Bem vindo {{ auth()->user()['name'] }}
                    </p>
                </div>

                <form action="{{ route('logout') }}">
                    <button type="submit"  class="btn btn-default" style="width:auto;position: absolute;right: 5%; top: 40%;">Sair</button>
                </form>
            </div>
        </div>
    @show
</header>

<main>
    @yield('navbar')
    <div class="row">
        <div class="col-md-2">
            <div class="sidebar-nav">
                <div class="navbar navbar-default" role="navigation">
                    <div class="smallHeader">
                        <a href="{{ route('dashboard') }}">
                            <img lass="img-responsive" src="{{ asset('img/LOGO.jpg') }}" alt="logo" style="height:100%; width:100%;border:0;">
                        </a>
                    </div>


                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <span class="visible-xs navbar-brand">Menu</span>
                    </div>
                    <div class="navbar-collapse collapse sidebar-navbar-collapse">

                        <ul class="nav navbar-nav">
                            {{-- array_key_exists('titulo', --}}
                            <li class="active"><a>Cadastros</a></li>
                            <li @if(View::getSections()['titulo'] == 'Clientes') class="active" @endif ><a href="{{ route('clientes')}}">Clientes</a></li>
                            <li @if(View::getSections()['titulo'] == 'Fornecedores') class="active" @endif ><a href="{{ route('fornecedores') }}">Fornecedor</a></li>
                            <li @if(View::getSections()['titulo'] == 'Produtos') class="active" @endif ><a href="{{  route('produtos') }}">Produtos</a></li>
                            <li @if(View::getSections()['titulo'] == 'Local de entrega') class="active" @endif ><a href="{{  route('local-entrega') }}">Local de entrega</a></li>
                            <li @if(View::getSections()['titulo'] == 'Contatos') class="active" ><a @else  ><a href="{{route('contatos') }}" @endif >Contatos</a></li>
                            <li class="active"><a>Serviços</a></li>
                            {{--<li @if(View::getSections()['titulo'] == 'Contratos') class="active" @endif ><a href="--}}{{--route('Contratos')--}}{{--">Contratos</a></li>--}}
                            <li @if(View::getSections()['titulo'] == 'OS') class="active" @endif ><a href="{{route('os')}}">Ordem de Serviço</a></li>
                            <li class="active"><a>Configurações</a></li>
                            <li @if(View::getSections()['titulo'] == 'Usuarios') class="active" @endif ><a href="{{route('usuarios')}}">Usuários</a></li>
                            <li @if(View::getSections()['titulo'] == 'Sobre') class="active" @endif ><a href="{{route('sobre')}}">Sobre</a></li>

                        </ul>

                    </div>
                </div>
            </div>
        </div>
        @show
        <div class="col-md-9">
            @yield('filter')
            @yield('content')
            @yield('content2')
            @yield('erros')
        </div>
    </div>
</main>

<footer>
    <hr>
    @section('footer')
        @include('menu.footer')
    @show
</footer>
</body>
</html>

