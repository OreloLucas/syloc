{{--<meta charset="utf-8">--}}
{{--<meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">


<!-- Styles -->
<link rel="stylesheet" href="{{ asset('css/general.css') }}">

<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
<link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">

<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">

<link rel="stylesheet" href="{{ asset('css/jasny-bootstrap.min.css') }}"> <!-- upload arquivo / mask -->

<!-- include the style -->
<link rel="stylesheet" href="{{ asset('css/alertify.min.css') }}" />
<!-- include a theme -->
<link rel="stylesheet" href="{{ asset('css/themes/default.min.css') }}"/>


<!-- Scripts -->


<script type="text/javascript" src="{{ asset('js/jquery-3.2.1.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-ui.js')}}"></script>

<script type="text/javascript" src="{{ asset('js/sweetalert.min.js')}}"></script>

<script type="text/javascript" src="{{ asset('js/alertify.min.js')}}"></script>

<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/dataTables.bootstrap.min.js')}}"></script>

<script type="text/javascript" src="{{ asset('js/jasny-bootstrap.min.js')}}"></script> <!-- upload arquivo / mask -->

<script type="text/javascript" src="{{ asset('js/general.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.js')}}"></script>


<a id="output"></a>
@if(isset(View::getSections()['titulo']))
    <script type="text/javascript">

        var s = document.createElement("script");
        s.type = "text/javascript";
        var base = "{{ asset('js/model/')}}";
        s.src = base + "/{{ View::getSections()['titulo'] }}.js";
        //console.log(s.src);
        s.innerHTML = null;
        s.id = "output";
        document.getElementById("output").innerHTML = "";
        document.getElementById("output").appendChild(s);
    </script>
@endif