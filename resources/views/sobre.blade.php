@extends('menu.header')

@section('titulo','Sobre')


@section('content')
    <table class="table table-bordered">
    <tr>
        <th>Licenciado para</th>
        <th> {{ $sobre->razao }}</th>
    </tr>
    <tr>
        <th>Versão do sistema</th>
        <th>{{ $sobre->versao }}</th>
    </tr>
    <tr>
        <th>Ultima atualização</th>
        <th>{{ $sobre->updated_at }}</th>
    </tr>
    <tr>
        <th>Cliente desde</th>
        <th>{{ $sobre->created_at }}</th>
    </tr>
    </table>
@endsection
