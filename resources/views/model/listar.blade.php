@extends('menu.header')

@section('titulo',$formName)

{{--filter--}}
{{--{{ dd($filter) }}--}}

@section('content')
    <div class="container" style="margin-left: -3%">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading" style="text-align: center">
                        <h4>{{$formHeader}}</h4>
                    </div>
                    <div class="panel-body">
                        <script>
                            $(document).ready(function () {
                                $('#TblListaModel').DataTable({
                                    "language": {
                                        "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
                                    }
                                });
                            });


                        </script>

                        <p><a class="btn btn-success" href="{{ route($routes['novo']) }}">
                                <span class="glyphicon glyphicon-plus"></span>
                                Novo(a) {{ $formHeader }}</a></p>

                        {{--<table class="table table-bordered table-condensed" >--}}
                        <table id="TblListaModel" class="row-border  display compact">
                            <thead>
                            <tr>
                                @foreach($header as $h)
                                    @if($h != 'acoes')
                                        <th>{{ $h }}</th>
                                    @else

                                    @endif


                                @endforeach
                                @if (in_array("acoes",$header))
                                    <th data-orderable="false" class="col-md-1">Ações</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($lista as $l)
                                <tr id="tr{{$l['id']}}">
                                    @foreach($body as $b)
                                        <td>
                                            @foreach(explode('|', $b) as $t)
                                                {{ $l[$t] }}
                                            @endforeach
                                        </td>
                                    @endforeach
                                    <td>
                                        @foreach($acoes as $a)
                                            @switch($a)
                                                @case('detalhe')
                                                <a href="{{ route($routes['detalhe'], $l['id'])  }}"
                                                   class="btn btn-info btn-xs"><span
                                                            class="glyphicon glyphicon-eye-open"></span></a>
                                                @break
                                                @case('editar')
                                                <a href="{{route($routes['editar'], $l['id']) }}"
                                                   class="btn btn-warning btn-xs"><span
                                                            class="glyphicon glyphicon-pencil"></span></a>
                                                @break
                                                @case('excluir')
                                                <a id="deletaConfirm"
                                                   data-id="{{ $l['id'] }}"
                                                   {{--data-route="{{ route($routes['excluir'],$l['id']) }}"--}}
                                                   data-deletaAJAX="{{ $routes['deletaAJAX'] }}"
                                                   data-text="{{ $excluir }}"
                                                   class="btn btn-danger btn-xs"><span
                                                            class="glyphicon glyphicon-trash"></span></a>
                                                @break
                                                {{--@case('OSit')--}}
                                                {{--<a id="OSit" href="{{route($routes['OSit'], $l['id']) }}"--}}
                                                   {{--data-id="{{ $l['id'] }}"--}}
                                                   {{--data-route="{{ route($routes['excluir'],$l['id']) }}"--}}
                                                   {{--data-deletaAJAX="{{ $routes['deletaAJAX'] }}"--}}
                                                   {{--data-text="{{ $excluir }}"--}}
                                                   {{--class="btn btn-bs btn-xs"><span--}}
                                                            {{--class="glyphicon glyphicon-th-list"></span></a>--}}
                                                {{--glyphicon glyphicon-tag--}}
                                                {{--glyphicon glyphicon-tags--}}
                                                {{--glyphicon glyphicon-th-list--}}
                                                {{--glyphicon glyphicon-barcode--}}
                                                {{--@break--}}
                                            @endswitch

                                        @endforeach
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    @if(Session::has('mensagem'))
                        <p class="{{ Session::get('mensagem')['class'] }} alert-fixed-bottom "
                           style="text-align: center">{{ Session::get('mensagem')['msg'] }}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection