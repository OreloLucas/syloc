@extends('menu.header')

@section('titulo',$formName)
@section('only_view','false')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-11 ">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5> Editar {{ $formHeader }} #{{$model->id}}</h5>
                    </div>
                    <div class="panel-body">
                        <form action="{{ route($routes['salvar']) }}" method="post" enctype='multipart/form-data'>
                            @include($form)
                            <button type="submit" class="btn btn-success col-md-1 col-md-offset-0">Salvar</button>
                            <a class="btn btn-danger col-md-1 col-md-offset-11" href="{{ route($routes['voltar']) }}">Voltar</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('erros')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection

