@extends('menu.header')

@section('titulo',$formName)
@section('only_view','true')

@section('content')
    <div class="container"
         {{--style="margin-left: -3%"--}}
    >
        <div class="row">
            {{--<div class="col-md-12 ">--}}
            <div class="col-md-11">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5>Detalhe {{ $formHeader }} #{{$model->id}} (Somente visualização)</h5>
                    </div>
                    <div class="panel-body">
                        <div id="readonly">
                            @include($form)
                            <a class="btn btn-danger col-md-1 col-md-offset-11" href="{{ route($routes['voltar']) }}">Voltar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

