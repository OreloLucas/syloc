# Syloc (provisótio)

* Sistema para focado para locação de materiais.

# instalação dev
* instalar git
* instalar psql / mysql
* instalar php storm (http://www.aku.vn/idea)
* php (thread safe)
* adicioanr diretório nas váriaveis de ambiente  'path'
* erro : vcruntime140.dll (instalar visual C++)
* instalar composer

# configuração dev

* git clone https://OreloLucas@bitbucket.org/OreloLucas/syloc.git
* editar .env com os parametros de comunicação com a base
* fazer o download do repositório vendor atualizado
* php artisan migrate (criar tabelas)
* php artisan serve (iniciar o servidor imbutido)

# instalação produção
* sem ambiente de produção no momento

# lib externas
* bootratap (https://getbootstrap.com/docs/3.3/)
* ~~sweet alert (https://sweetalert.js.org/)~~
* alertify (http://alertifyjs.com/)
* data tables (https://datatables.net/)
* jansy (http://www.jasny.net/bootstrap/)
* jquery (https://jquery.com/)
* jquery-ui (http://jqueryui.com/)
* ~~ phantomjs (http://jonnnnyw.github.io/php-phantomjs/4.0/) ~~
* dompdf (https://github.com/barryvdh/laravel-dompdf)


# Mudanças vendor
#### syloc\vendor\laravel\framework\src\Illuminate\Database\Eloquent\Concerns\HasAttributes.php
* fromDateTime() - ajuste formatação (internaciolização de db)
* serializeDate() - remoção tipagem do metodo (serializable)

#### syloc\vendor\nesbot\carbon\src\Carbon\Carbon.php 
* createFromFormat() - formatação da data (internaciolização de db)


