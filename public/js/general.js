/*
 * Validao preenchiento da senha no reset
 * */
function valida() {
    var pw1 = document.getElementById('password1').value.toString();
    var pw2 = document.getElementById('password2').value.toString();

    if ((pw1 !== "" && pw2 !== "")) {
        if (pw1 === pw2) {
            document.getElementById("submit").disabled = false;
            document.getElementById('submit_btn').innerHTML = "";
        } else {
            document.getElementById("submit").disabled = true;
            document.getElementById('submit_btn').innerHTML = "Senha diferentes";
        }
    } else {
        document.getElementById("submit").disabled = true;
        document.getElementById('submit_btn').innerHTML = "";
    }
}

/*
 * cria modal para deleta registros
 * */
$(document).on('click', '#deletaConfirm', function (e) {
    var id = $(this).data('id');
    var route = $(this).data('deletaajax');
    //console.log(route);
    alertify.confirm('Deletar registro ?', 'Após deletar não há como voltar')
        .set({labels: {ok: 'Deletar', cancel: 'Cacelar'}})
        .set('reverseButtons', true)
        .set('onok', function () {
            showPleaseWait();
            $.ajaxSetup({
                headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
            });
            $.ajax({
                type: "post",
                url: route, //'/cliente/deleta/',
                dataType: "json",
                data: {
                    id: id
                },
                success: function (data) {
                    // TblListaModel
                    var t = $('#TblListaModel').DataTable();
                    document.getElementById("tr" + id).classList.add('selected');
                    t.row('.selected').remove().draw(false);
                    hidePleaseWait();
                    alertify.success('Excluido');
                },
                error: function () {
                    hidePleaseWait();
                    alertify.error('erro');
                }
            });
        })
        .set('oncancel', function () {
            alertify.error('Cancelado');
        });
});



function disable_if_readonly() {
    document.getElementById("readonly").disabled = true;
    var nodes = document.getElementById("readonly").getElementsByTagName('*');
    for (var i = 0; i < nodes.length; i++) {
        nodes[i].readOnly = true;
    }
}

function formatDecimal(element) {
    var v;
    v = parseFloat(element.value);
    element.value = parseFloat(Math.round(v * 100) / 100).toFixed(2);
}

function addEvent(element, evnt, funct) {
    if (element.attachEvent)
        return element.attachEvent('on' + evnt, funct);
    else
        return element.addEventListener(evnt, funct, false);
}


function showPleaseWait() {
    $("#load").modal("show");
}

function hidePleaseWait() {
    $("#load").modal("hide");
}

function showSPleaseWait() {
    $("#sLoad").modal("show");
}

function hideSPleaseWait() {
    $("#sLoad").modal("hide");
}


$.get("/modal/sLoadModal", function (html) {
    $(document.body).append(html);
    $("#sLoad").modal("hide");
}, 'html');

$.get("/modal/LoadModal", function (html) {
    $(document.body).append(html);
    $("#load").modal("hide");
}, 'html');

function appendJS(js){
    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src = js;
    s.innerHTML = null;
    s.id = "output";
    document.getElementById("output").innerHTML = "";
    document.getElementById("output").appendChild(s);
}

function loadAddProduto(){
    if ($("#produtoAddToOSModal").text() == '') {
        $.get("/modal/produtoAddToOSModal", function (html) {
            $(document.body).append(html);
        }, 'html');
    }
}

// function createGroup(idPai) {
//     var divPai = document.getElementById(idPai);
//     var str = "<div class=\"row form-group\"></div>";
//     divPai.insertAdjacentHTML('beforeend', str);
// }
//
// function createLInput(divPai, divClazz, label, type, name, id, clazz, placeholder, value, old) {
//     var divPai = document.getElementById(divPai);
//
//     var finalValue = value;
//     if (value == "") {
//         finalValue = old;
//     }
//
//     var html = "<div class=\"" + divClazz + "\">" +
//         "<label>" + label + "</label>" +
//         "<input type=\"" + type + "\"" +
//         " name=\"" + name + "\"" +
//         " id=\"" + id + "\"" +
//         " class=\"" + clazz + "\"" +
//         " placeholder=\"" + name + "\"" +
//         " name=\"" + name + "\"" +
//         " placeholder=\"" + placeholder + "\"" +
//         " value=\"" + finalValue + "\"" +
//         " </div>";
//     divPai.insertAdjacentHTML('beforeend', html);
// }