var modelName  = 'fornecedor';

function vinculaContatoF() {
    vinculaContato(modelName);
}

function desvinculaContatoF(idcontato) {
    desvinculaContato(idcontato,modelName);
}

$(document).on('click', '#vinculaContato', function (e) {
    vinculaContatoF();
});

/*
 * Verifica se o campo 'pj' está marcado ou não, inficando se o mesmo é uma pessoa fisica, ou juridica
 * marcado      - fisica    (necessita CPF)
 * desmarcado   - juridica  (necesita CNPJ)
 * */
function isPessoaFisica(element) {
    console.log(element.checked);
    if (element.checked) {
        document.getElementById("pj").style.visibility = 'hidden';
        document.getElementById("pf").style.visibility = 'visible';
    } else {
        document.getElementById("pj").style.visibility = 'visible';
        document.getElementById("pf").style.visibility = 'hidden';
    }
}

function showAddContato() {
    //ler html from file
    if ($("#contatoModal").text() == '') {
        $.get("/modal/contatoModal/fornecedor", function (html) {
            // console.log($("#contatoModal").text());
            $(document.body).append(html);
            $("#contatoModal").modal("show");
        }, 'html');
    }// or 'text', 'xml', 'more'
    else {
        $("#contatoModal").modal("show");
    }

}

function hideAddContato() {
    $("#contatoModal").modal("hide");
}
