/*
 * busca contatos com base no valor do input
 * */

function buscaContatoF(value, clienteid) {
    $("#buscaContato").autocomplete({
        source: function (request, response) {
            $.ajaxSetup({
                headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
            });
            $.ajax({
                type: "post",
                url: '/fornecedor/gettotodos',
                dataType: "json",
                data: {
                    filtro: value.toString(),
                    fornecedor_id: clienteid
                },
                success: function (data) {
                    var values = $.map(data, function (item) {
                        return {uri: item.nome, label: item.nome, value: item.nome, id: item.id};
                        console.log(item.nome);
                    });
                    response(values);
                }
            });
        },
        minLength: 3,
        autoFocus: true,
        select: function (event, ui) {
            //console.log(ui.item.label + ' ' + ui.item.id);
            vinculaContatoF(ui.item.id, clienteid);
            $("ul.ui-autocomplete").hide();
            event.preventDefault();
            $("#buscaContato").val('');
        },
        close: function () {
//                        console.log('saiu');
            if (value.length >= 3) {
                if (!$("ul.ui-autocomplete").is(":visible")) {
                    $("ul.ui-autocomplete").show();
                }
            }
        }
    });

}

/*
 * Verifica se o campo 'pj' está marcado ou não, inficando se o mesmo é uma pessoa fisica, ou juridica
 * marcado      - fisica    (necessita CPF)
 * desmarcado   - juridica  (necesita CNPJ)
 * */
function isPessoaFisica(element) {
    console.log(element.checked);
    if (element.checked) {
        document.getElementById("pj").style.visibility = 'hidden';

        document.getElementById("pf").style.visibility = 'visible';
    } else {
        document.getElementById("pj").style.visibility = 'visible';

        document.getElementById("pf").style.visibility = 'hidden';
    }

}

function vinculaContatoF(idcontato, idcliente) {
    var cliente = idcliente;//$(this).data('cliente');
    var contato = idcontato;//$(this).data('contato');
    console.log(cliente + ' ' + contato);
    //e.preventDefault();
    swal({
        title: 'Carregando ...',
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false,
        allowEscapeKey: false
    });

    $.ajaxSetup({
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
    });

    $.ajax({
        type: 'POST',
        url: '/fornecedor/vinculaContato',
        dataType: "json",
        data: {
            'fornecedor_id': cliente,
            'contato_id': contato
        },
        success: function (data) {
            swal("Vinculado!", "Contato vinculado ao fornecedor.", "success");
            var t = $('#TblThisContatos').DataTable();
            var table_rows =
                '<tr id="tr' + contato + '">' +
                '<td>' + data['nome'] + '</td>' +
                '<td>' + data['email'] + '</td>' +
                '<td>' + data['fone1'] + '</td>' +
                '<td>' + data['fone2'] + '</td>' +
                '<td>' + data['fone3'] + '</td>' +
                '<td>' +
                '<a id="desvincula"' +
                ' onclick="desvinculaContatoF(' + contato + ')"' +
                ' class="btn btn-danger btn-xs glyphicon glyphicon-trash">' +
                '</a>' +
                '</td>' +
                '</tr>';
            t.rows.add($(table_rows)).draw();
        }, error: function () {
            swal("ERRO", "ERRO não catalogado", "error");
            console.log("erro");
        }
    });
}

function desvinculaContatoF(idcontato) {
    var contato = idcontato;

    swal({
            title: 'Desvincular contato',
            text: 'Deseja desvincular contato ?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Desvincular!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false,
            allowOutsideClick: true,
            allowEscapeKey: true,
            showLoaderOnConfirm: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajaxSetup({
                    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
                });

                $.ajax({
                    type: 'POST',
                    url: '/fornecedor/desvinculaContato/',
                    dataType: "json",
                    data: {
                        'contato_id': contato
                    },
                    success: function (data) {
                        swal("Desvinculado!", "Contato desvinculado ao fornecedor.", "success");
                        console.log("sucesso");
                        // document.getElementsByName("desvincula" + contato).item(0).style.visibility = 'hidden';
                        // document.getElementsByName("vincula" + contato).item(0).style.visibility = 'visible';

                        var t = $('#TblThisContatos').DataTable();
                        document.getElementById("tr" + contato).classList.add('selected');
                        t.row('.selected').remove().draw(false);

                    }, error: function () {
                        swal("ERRO", "ERRO não catalogado", "error");
                        console.log("erro");
                    }
                });
            } else {
                swal("Cancelar", "Operação  abortada", "error");
            }
        });
}