var modelName  = 'cliente';

function vinculaContatoC() {
    vinculaContato('cliente');
}

function desvinculaContatoC(idcontato) {
    desvinculaContato(idcontato,modelName);
}

$(document).on('click', '#vinculaContato', function (e) {
    vinculaContatoC();
});

/*
 * Verifica se o campo 'pj' está marcado ou não, inficando se o mesmo é uma pessoa fisica, ou juridica
 * marcado      - fisica    (necessita CPF)
 * desmarcado   - juridica  (necesita CNPJ)
 * */
function isPessoaFisica (element) {
    console.log(element);
    if (element) {
        document.getElementById("pj").style.visibility = 'hidden';
        document.getElementById("pf").style.visibility = 'visible';
        document.getElementById("LblRazao").innerHTML  = 'Nome*';
        document.getElementById("LblFantasia").innerHTML  = 'Apelido';
        document.getElementById("LblFantasia").style.visibility = 'hidden';
        document.getElementById("fantasia").style.visibility = 'hidden';
    } else {
        document.getElementById("pj").style.visibility = 'visible';
        document.getElementById("pf").style.visibility = 'hidden';
        document.getElementById("LblRazao").innerHTML  = 'Razão Social*';
        document.getElementById("LblFantasia").innerHTML  = 'Fantasia';
        document.getElementById("LblFantasia").style.visibility = 'visible';
        document.getElementById("fantasia").style.visibility = 'visible';
    }
}


function showAddContato() {
    if ($("#contatoModal").text() == '') {
        $.get("/modal/contatoModal/cliente", function (html) {
            // console.log($("#contatoModal").text());
            $(document.body).append(html);
            $("#contatoModal").modal("show");
        }, 'html');
    }
    else {
        $("#contatoModal").modal("show");
    }
}

function hideAddContato() {
    $("#contatoModal").modal("hide");
}


