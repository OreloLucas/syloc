/*
 * busca contatos com base no valor do input
 * */

function buscaContatoC(value) {
    $("#buscacontato").autocomplete({
        source: function (request, response) {
            console.log("buscando");
            $.ajaxSetup({
                headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
            });
            $.ajax({
                type: "post",
                url: '/cliente/gettotods',
                dataType: "json",
                data: {
                    filtro: value.toString(),
                    //cliente_id: clienteid
                },
                success: function (data) {
                    console.log("buscou");
                    var values = $.map(data, function (item) {
                        return {
                            uri: item.nome, label: item.nome, value: item.nome, id: item.id,
                            nome: item.nome, rg: item.rg, cpf: item.cpf,
                            email: item.email, fone1: item.fone1, fone2: item.fone2
                        };
                        //console.log(item.nome);
                    });
                    response(values);
                }
            });
        },
        minLength: 3,
        autoFocus: true,
        appendTo: ".modalContato",
        select: function (event, ui) {
            //desabilita inputs modal
            document.getElementById('nomeContato').disabled = true;
            document.getElementById('rgContato').disabled = true;
            document.getElementById('cpfContato').disabled = true;
            document.getElementById('emailContato').disabled = true;
            document.getElementById('fone1Contato').disabled = true;
            document.getElementById('fone2Contato').disabled = true;
            document.getElementById('buscacontato').disabled = true;
            document.getElementById('reset').style = "display:block";
            //preenche campos modal
            document.getElementById('idContato').value = ui.item.id;
            document.getElementById('nomeContato').value = ui.item.nome;
            document.getElementById('buscacontato').value = ui.item.nome;
            document.getElementById('rgContato').value = ui.item.rg;
            document.getElementById('cpfContato').value = ui.item.cpf;
            document.getElementById('emailContato').value = ui.item.email;
            document.getElementById('fone1Contato').value = ui.item.fone1;
            document.getElementById('fone2Contato').value = ui.item.fone2;

            //inclui
            //console.log(ui.item.label + ' ' + ui.item.id);
            //vinculaContatoC(ui.item.id, clienteid);
            $("ul.ui-autocomplete").hide();
            //event.preventDefault();
            //$("#buscacontato").val('');
        },
        close: function () {
            // console.log('saiu');
            if (value.length >= 3) {
                if (!$("ul.ui-autocomplete").is(":visible")) {
                    $("ul.ui-autocomplete").show();
                }
            }
        }
    });

}

/*
 * Verifica se o campo 'pj' está marcado ou não, inficando se o mesmo é uma pessoa fisica, ou juridica
 * marcado      - fisica    (necessita CPF)
 * desmarcado   - juridica  (necesita CNPJ)
 * */
function isPessoaFisica(element) {
    //console.log(element.checked);
    if (element.checked) {
        document.getElementById("pj").style.visibility = 'hidden';

        document.getElementById("pf").style.visibility = 'visible';
    } else {
        document.getElementById("pj").style.visibility = 'visible';

        document.getElementById("pf").style.visibility = 'hidden';
    }

}


function vinculaContatoC(idcontato, idcliente) {
    // var cliente = idcliente;//$(this).data('cliente');
    // var contato = idcontato;//$(this).data('contato');
    //verificar se existe id no html
    //console.log(cliente + ' ' + contato);
    //e.preventDefault();
    swal({
        title: 'Carregando ...',
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false,
        allowEscapeKey: false
    });
    var data;
    $.ajaxSetup({
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
    });
    console.log(document.getElementById('idContato').value + '');
    if (parseInt(document.getElementById('idContato').value) == 0) {
        console.log('novo contato');
        data = {
            id: 0,
            cliente_id: document.getElementById('id').value,
            nome: document.getElementById('nomeContato').value,
            rg: document.getElementById('rgContato').value,
            cpf: document.getElementById('cpfContato').value,
            email: document.getElementById('emailContato').value,
            fone1: document.getElementById('fone1Contato').value,
            fone2: document.getElementById('fone2Contato').value
        };
        //sem id, novo contato
    } else {
        console.log(' contato existente');
        // document.getElementById('idContato').value
        data = {
            id: document.getElementById('idContato').value,
            cliente_id: document.getElementById('id').value,
        };
    }

    $.ajax({
        type: 'POST',
        url: '/cliente/vinculaContato',
        dataType: "json",
        data: data
        //     {
        //     'cliente_id': cliente,
        //     'contato_id': contato
        // }
        ,
        success: function (data) {
            swal("Vinculado!", "Contato vinculado ao cliente.", "success");
            var t = $('#TblThisContatos').DataTable();
            var table_rows =
                '<tr id="tr' +  data['id'] + '">' +
                '<td>' + data['nome'] + '</td>' +
                '<td>' + data['email'] + '</td>' +
                '<td>' + data['fone1'] + '</td>' +
                '<td>' + data['fone2'] + '</td>' +
                '<td>' + data['fone3'] + '</td>' +
                '<td>' +
                '<a id="desvincula"' +
                ' onclick="desvinculaContatoC(' +  data['id'] + ')"' +
                ' class="btn btn-danger btn-xs glyphicon glyphicon-trash">' +
                '</a>' +
                '</td>' +
                '</tr>';
            t.rows.add($(table_rows)).draw();
            resetModal();
            // document.getElementsByName("desvincula" + contato).item(0).style.visibility = 'visible'; //modal
            // document.getElementsByName("vincula" + contato).item(0).style.visibility = 'hidden';     //modal
        }, error: function () {
            swal("ERRO", "ERRO não catalogado", "error");
            console.log("erro");
        }
    });
}

function desvinculaContatoC(idcontato) {
    var contato = idcontato;//$(this).data('contato');

    //e.preventDefault();
    swal({
            title: 'Desvincular contato',
            text: 'Deseja desvincular contato ?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Desvincular!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false,
            allowOutsideClick: true,
            allowEscapeKey: true,
            showLoaderOnConfirm: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajaxSetup({
                    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
                });

                $.ajax({
                    type: 'POST',
                    url: '/cliente/desvinculaContato/',
                    dataType: "json",
                    data: {
                        'contato_id': contato
                    },
                    success: function (data) {
                        swal("Desvinculado!", "Contato desvinculado ao cliente.", "success");
                        console.log("sucesso");
                        // document.getElementsByName("desvincula" + contato).item(0).style.visibility = 'hidden';
                        // document.getElementsByName("vincula" + contato).item(0).style.visibility = 'visible';

                        var t = $('#TblThisContatos').DataTable();
                        document.getElementById("tr" + contato).classList.add('selected');
                        t.row('.selected').remove().draw(false);

                    }, error: function () {
                        swal("ERRO", "ERRO não catalogado", "error");
                        console.log("erro");
                    }
                });
            } else {
                swal("Cancelar", "Operação  abortada", "error");
            }
        });
};


// /*
//  * MODAL - cria modal para vincular registros
//  * */
//
// $(document).on('click', '#vincula', function (e) {
//     var cliente = $(this).data('cliente');
//     var contato = $(this).data('contato');
//
//     //e.preventDefault();
//     swal({
//         title: 'Carregando ...',
//         showCancelButton: false,
//         showConfirmButton: false,
//         allowOutsideClick: false,
//         allowEscapeKey: false
//     });
//
//     $.ajaxSetup({
//         headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
//     });
//
//     $.ajax({
//         type: 'POST',
//         url: '/cliente/vinculaContato',
//         dataType: "json",
//         data: {
//             'cliente_id': cliente,
//             'contato_id': contato
//         },
//         success: function (data) {
//             swal("Vinculado!", "Contato vinculado ao cliente.", "success");
//             var t = $('#TblThisContatos').DataTable();
//             var table_rows =
//                 '<tr id="tr' + contato + '">' +
//                 '<td>' + data['nome'] + '</td>' +
//                 '<td>' + data['email'] + '</td>' +
//                 '<td>' + data['fone1'] + '</td>' +
//                 '<td>' + data['fone2'] + '</td>' +
//                 '<td>' + data['fone3'] + '</td>' +
//                 '<td>' +
//                 '<a id="desvincula"' +
//                 ' data-cliente="' + cliente + '"' +
//                 ' data-contato="' + contato + '"' +
//                 ' class="btn btn-danger btn-xs glyphicon glyphicon-trash">' +
//                 '</a>' +
//                 '</td>' +
//                 '</tr>';
//             t.rows.add($(table_rows)).draw();
//             document.getElementsByName("desvincula" + contato).item(0).style.visibility = 'visible'; //modal
//             document.getElementsByName("vincula" + contato).item(0).style.visibility = 'hidden';     //modal
//         }, error: function () {
//             swal("ERRO", "ERRO não catalogado", "error");
//             console.log("erro");
//         }
//     });
// });
//
// /*
//  * MODAL - cria modal para desvincular registros
//  * */
// $(document).on('click', '#desvincula', function (e) {
//     var contato = $(this).data('contato');
//
//     //e.preventDefault();
//     swal({
//             title: 'Desvincular contato',
//             text: 'Deseja desvincular contato ?',
//             type: "warning",
//             showCancelButton: true,
//             confirmButtonColor: "#DD6B55",
//             confirmButtonText: "Desvincular!",
//             cancelButtonText: "Cancelar",
//             closeOnConfirm: false,
//             closeOnCancel: false,
//             allowOutsideClick: true,
//             allowEscapeKey: true,
//             showLoaderOnConfirm: true
//         },
//         function (isConfirm) {
//             if (isConfirm) {
//                 $.ajaxSetup({
//                     headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
//                 });
//
//                 $.ajax({
//                     type: 'POST',
//                     url: '/cliente/desvinculaContato/',
//                     dataType: "json",
//                     data: {
//                         'contato_id': contato
//                     },
//                     success: function (data) {
//                         swal("Desvinculado!", "Contato desvinculado ao cliente.", "success");
//                         console.log("sucesso");
//                         // document.getElementsByName("desvincula" + contato).item(0).style.visibility = 'hidden';
//                         // document.getElementsByName("vincula" + contato).item(0).style.visibility = 'visible';
//
//                         var t = $('#TblThisContatos').DataTable();
//                         document.getElementById("tr" + contato).classList.add('selected');
//                         t.row('.selected').remove().draw(false);
//
//                     }, error: function () {
//                         swal("ERRO", "ERRO não catalogado", "error");
//                         console.log("erro");
//                     }
//                 });
//             } else {
//                 swal("Cancelar", "Operação  abortada", "error");
//             }
//         });
// });


function showAddContato(id) {
    //ler html from file
    if ($("#contatoModal").text() == '') {
        $.get("/cliente/contatoModal", function (html) {
            // console.log($("#contatoModal").text());
            $(document.body).append(html);
            $("#contatoModal").modal("show");
        }, 'html');
    }// or 'text', 'xml', 'more'
    else {
        $("#contatoModal").modal("show");
    }

}

/**
 * Hides "Please wait" overlay. See function showPleaseWait().
 */
function hideAddContato() {
    $("#contatoModal").modal("hide");
}

function resetModal() {
    //desabilita inputs modal
    document.getElementById('nomeContato').disabled = false;
    document.getElementById('rgContato').disabled = false;
    document.getElementById('cpfContato').disabled = false;
    document.getElementById('emailContato').disabled = false;
    document.getElementById('fone1Contato').disabled = false;
    document.getElementById('fone2Contato').disabled = false;
    document.getElementById('buscacontato').disabled = false;
    document.getElementById('reset').style = "display:none";
    // //preenche campos modal
    document.getElementById('nomeContato').value = '';
    document.getElementById('rgContato').value = '';
    document.getElementById('cpfContato').value = '';
    document.getElementById('emailContato').value = '';
    document.getElementById('fone1Contato').value = '';
    document.getElementById('fone2Contato').value = '';
    document.getElementById('buscacontato').value = '';
}