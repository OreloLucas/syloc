function buscaContato(value, modelName) {
    console.log(modelName);
    document.getElementById("buscacontato").style.backgroundColor = "";
    $("#buscacontato").autocomplete({
        source: function (request, response) {
            console.log("buscando");
            $.ajaxSetup({
                headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
            });
            $.ajax({
                type: "post",
                url: '/' + modelName + '/gettotodos',
                dataType: "json",
                data: {
                    filtro: value.toString()
                },
                success: function (data) {
                    var values;
                    if (data == '') {
                        console.log("buscou vazio");
                        document.getElementById("buscacontato").style.backgroundColor = "#ff5c33";
                    } else {
                        console.log("buscou");
                        values = $.map(data, function (item) {
                            return {
                                uri: item.nome, label: item.nome, value: item.nome, id: item.id,
                                nome: item.nome, rg: item.rg, cpf: item.cpf,
                                email: item.email, fone1: item.fone1, fone2: item.fone2, fone3: item.fone3
                            };
                        });
                    }
                    response(values);
                }
            });
        },
        minLength: 3,
        autoFocus: true,
        appendTo: ".modalContato",
        select: function (event, ui) {
            //desabilita inputs modal
            document.getElementById('nomeContato').disabled = true;
            document.getElementById('rgContato').disabled = true;
            document.getElementById('cpfContato').disabled = true;
            document.getElementById('emailContato').disabled = true;
            document.getElementById('fone1Contato').disabled = true;
            document.getElementById('fone2Contato').disabled = true;
            document.getElementById('fone3Contato').disabled = true;
            document.getElementById('buscacontato').disabled = true;
            document.getElementById('reset').style = "display:block";
            //preenche campos modal
            document.getElementById('idContato').value = ui.item.id;
            document.getElementById('nomeContato').value = ui.item.nome;
            document.getElementById('buscacontato').value = ui.item.nome;
            document.getElementById('rgContato').value = ui.item.rg;
            document.getElementById('cpfContato').value = ui.item.cpf;
            document.getElementById('emailContato').value = ui.item.email;
            document.getElementById('fone1Contato').value = ui.item.fone1;
            document.getElementById('fone2Contato').value = ui.item.fone2;
            document.getElementById('fone3Contato').value = ui.item.fone3;
            $("ul.ui-autocomplete").hide();
        },
        close: function () {
            if (value.length >= 3) {
                if (!$("ul.ui-autocomplete").is(":visible")) {
                    $("ul.ui-autocomplete").show();
                }
            }
        }
    });
}

function vinculaContato(modelName) {
    showPleaseWait();
    var data;
    $.ajaxSetup({
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
    });
    if (parseInt(document.getElementById('idContato').value) == 0) { //novo contato
        data = {
            id: 0,
            nome: document.getElementById('nomeContato').value,
            // rg: document.getElementById('rgContato').value,
            cpf: document.getElementById('cpfContato').value
            // email: document.getElementById('emailContato').value,
            // fone1: document.getElementById('fone1Contato').value,
            // fone2: document.getElementById('fone2Contato').value,
            // fone3: document.getElementById('fone3Contato').value
        };
    } else { //contato existe
        data = {
            id: document.getElementById('idContato').value
        };
    }

    switch (modelName) {
        case 'cliente':
            data.cliente_id = document.getElementById('id').value;
            data.fornecedores_id = 0;
            break;
        case 'fornecedor':
            data.fornecedores_id = document.getElementById('id').value;
            data.cliente_id = 0;
            break;
    }

    console.log(data);
    $.ajax({
        type: 'POST',
        url: '/' + modelName + '/vinculaContato',
        dataType: "json",
        data: data,
        success: function (data) {

            var t = $('#TblThisContatos').DataTable();
            var table_rows =
                '<tr id="tr' + data['id'] + '">' +
                '<td>' + (data['nome'] == null ? '' : data['nome']) + '</td>' +
                '<td>' + (data['email'] == null ? '' : data['email']) + '</td>' +
                '<td>' + (data['fone1'] == null ? '' : data['fone1']) + '</td>' +
                '<td>' + (data['fone2'] == null ? '' : data['fone2']) + '</td>' +
                '<td>' + (data['fone3'] == null ? '' : data['fone3']) + '</td>' +
                '<td>' +
                '<a id="desvincula"' +
                ' onclick="desvinculaContato' + modelName[0].toUpperCase() + '(' + data['id'] + ')"' +
                ' class="btn btn-danger btn-xs glyphicon glyphicon-trash">' +
                '</a>' +
                '</td>' +
                '</tr>';
            t.rows.add($(table_rows)).draw();
            resetModal();
            hidePleaseWait();
            document.getElementById('idContato').value = 0;
            alertify.success('Contato vinculado com sucesso');
        }, error: function (e) {
            var errors = JSON.stringify(e['responseJSON']['errors']);

            var jsonObj = $.parseJSON(errors);
            var arr = Object.keys(jsonObj);

            for (var i = 0; i < arr.length; i++) {
                console.log(jsonObj[arr[i]].toString());
                alertify.error(jsonObj[arr[i]].toString());
            }
            //resetModal();
            hidePleaseWait();
        }
    });
}

function desvinculaContato(idcontato, modelName) {
    var contato = idcontato;
    alertify.confirm('Desvincular contato', 'Deseja desvincular contato ?')
        .set({labels: {ok: 'Desvincular', cancel: 'Cacelar'}})
        .set('onok', function () {
            showPleaseWait();
            $.ajaxSetup({
                headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
            });
            $.ajax({
                type: 'POST',
                url: '/' + modelName + '/desvinculaContato/',
                dataType: "json",
                data: {
                    'contato_id': contato
                },
                success: function (data) {
                    var t = $('#TblThisContatos').DataTable();
                    document.getElementById("tr" + contato).classList.add('selected');
                    t.row('.selected').remove().draw(false);
                    alertify.success('Contato desvinculado com sucesso');
                    hidePleaseWait();
                }, error: function () {
                    hidePleaseWait();
                    alertify.error('ERRO não catalogado');
                }
            });
        })
        .set('oncancel', function () {
            alertify.error('Cancelado');
        });
}

function resetModal() {
    //desabilita inputs modal
    document.getElementById('nomeContato').disabled = false;
    document.getElementById('rgContato').disabled = false;
    document.getElementById('cpfContato').disabled = false;
    document.getElementById('emailContato').disabled = false;
    document.getElementById('fone1Contato').disabled = false;
    document.getElementById('fone2Contato').disabled = false;
    document.getElementById('fone3Contato').disabled = false;
    document.getElementById('buscacontato').disabled = false;
    document.getElementById('reset').style = "display:none";
    // //preenche campos modal
    document.getElementById('nomeContato').value = '';
    document.getElementById('rgContato').value = '';
    document.getElementById('cpfContato').value = '';
    document.getElementById('emailContato').value = '';
    document.getElementById('fone1Contato').value = '';
    document.getElementById('fone2Contato').value = '';
    document.getElementById('fone3Contato').value = '';
    document.getElementById('buscacontato').value = '';
    document.getElementById('idContato').value = 0;
}