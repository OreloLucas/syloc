/**
 * Created by Lucas on 09/08/2017.
 */
function buscaCliente(value) {
    $("#buscacliente").autocomplete({
        source: function (request, response) {
            console.log("buscando");
            $.ajaxSetup({
                headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
            });
            $.ajax({
                type: "post",
                url: '/utils/clientes',
                dataType: "json",
                data: {
                    filtro: value.toString()
                },
                success: function (data) {
                    console.log("buscou");
                    var values = $.map(data, function (item) {
                        return {
                            uri: item.razao, label: item.razao, value: item.razao, id: item.id,
                            razao: item.razao, cnpj: item.cnpj
                            //, rg: item.rg, cpf: item.cpf,
                            // email: item.email, fone1: item.fone1, fone2: item.fone2,fone3: item.fone3
                        };
                        //console.log(item.nome);
                    });
                    response(values);
                }
            });
        },
        minLength: 3,
        autoFocus: true,
        //appendTo: ".modalContato",
        select: function (event, ui) {
            //preenche campos modal
            document.getElementById('id_cliente').value = ui.item.id;
            document.getElementById('cliente').value = ui.item.razao;
            document.getElementById('cnpj').value = ui.item.cnpj;

            //reset autocomplete
            ui.item.value = "";
            $("ul.ui-autocomplete").hide();
        },
        close: function () {
            if (value.length >= 3) {
                if (!$("ul.ui-autocomplete").is(":visible")) {
                    $("ul.ui-autocomplete").show();
                }
            }
        }
    });
}

function buscaLocalEntrega(value) {
    $("#buscalocal").autocomplete({
        source: function (request, response) {
            console.log("buscando");
            $.ajaxSetup({
                headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
            });
            $.ajax({
                type: "post",
                url: '/utils/localentrega',
                dataType: "json",
                data: {
                    filtro: value.toString()
                },
                success: function (data) {
                    console.log("buscou");
                    var values = $.map(data, function (item) {
                        return {
                            uri: item.nome, label: item.nome, value: item.nome,
                            id: item.id, nome_entrega: item.nome,
                            endereco: item.endereco, numero: item.numero, bairro: item.bairro,
                            cidade: item.cidade, UF: item.UF//,
                            //CEP: item.CEP, complemento: item.complemento
                        };
                    });
                    response(values);
                }
            });
        },
        minLength: 3,
        autoFocus: true,
        //appendTo: ".modalContato",
        select: function (event, ui) {
            //preenche campos modal
            //localentrega
            document.getElementById('localentrega').innerHTML =
                ui.item.nome_entrega + '\n' +
                ui.item.endereco + ', ' + ui.item.numero + '\n' + // ' - ' + ui.item.CEP
                ui.item.bairro + ' - ' + ui.item.cidade + ' - (' + ui.item.UF.toUpperCase() + ')';
            // ui.item.CEP + ' - ' + ui.item.complemento ;

            document.getElementById('id_local_entrega').value = ui.item.id;
            //
            // document.getElementById('entrega_nome').value = ui.item.nome_entrega;
            // document.getElementById('entrega_endereco').value = ui.item.endereco;
            // document.getElementById('entrega_numero').value = ui.item.numero;
            // document.getElementById('entrega_bairro').value = ui.item.bairro;
            // document.getElementById('entrega_cidade').value = ui.item.cidade;
            // document.getElementById('entrega_UF').value = ui.item.UF.toUpperCase();
            // document.getElementById('entrega_reset').style = "display:compact";

            //reset autocomplete
            ui.item.value = "";
            $("ul.ui-autocomplete").hide();
        },
        close: function () {
            if (value.length >= 3) {
                if (!$("ul.ui-autocomplete").is(":visible")) {
                    $("ul.ui-autocomplete").show();
                }
            }
        }
    });
}

// function resetLocalEntrega() {
//     document.getElementById('id_local_entrega').value = 0;
//     document.getElementById('entrega_nome').value = '';
//     document.getElementById('entrega_rua').value = '';
//     document.getElementById('entrega_bairro').value ='';
//     document.getElementById('entrega_cidade').value = '';
//     document.getElementById('entrega_UF').value = '';
//     // document.getElementById('entrega_reset').style = "display:none";
// }



function ajustaHora(obj) {
    var hora = "";
    var v = parseInt(obj.value[0] + obj.value[1]);

    if (v > 23) {
        hora = "23";
    } else {
        hora = v.toString();
    }
    if (hora.length != 2) {
        hora = '0' + hora;
    }
    v = parseInt(obj.value[3] + obj.value[4]);

    var min = '';
    if (v > 59) {
        min = '59';
    } else if (isNaN(v)) {
        min = '00';
    }
    else {
        min = v.toString();
    }
    if (min.length != 2) {
        min = '0' + min;
    }
    document.getElementById(obj.id).value = hora + ':' + min;
}

$(function () {
    var option = {
        changeMonth: true,
        changeYear: true,
        navigationAsDateFormat: true,
        dateFormat: 'dd/mm/yy',
        dayNamesMin: ["Seg", "Ter", "Qua", "Qui", "Sex", "Sab", "Dom"],
        dayNames: ["Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sabado", "Domingo"],
        dayNamesShort: ["Seg", "Ter", "Qua", "Qui", "Sex", "Sab", "Dom"],
        monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
        monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]
    };
    $("#dtevento").datepicker(option);
    $("#dtentrega").datepicker(option);
    $("#dtrecolhimento").datepicker(option);

});

var produto_id, os_id, preco_loca;
function addToOS() {
    //QUANTIDADE
    //CONFIRMA PRECO
    showPleaseWait();
    var qtd = document.getElementById('modal_qtd').value;
    var preco_loca = document.getElementById('modal_preco_loca').value;

    // console.log('add to os ' + produto + ' ' + OS);
    $.ajaxSetup({
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
    });
    $.ajax({
        type: "POST",
        url: '/ordem-servico/addProduto/',
        dataType: "json",
        data: {
            id_prod: produto_id,
            id_os: os_id,
            qtd: qtd,
            preco_loca: preco_loca
        },
        success: function (data) {
            // console.log(data);
            console.log('sucesso');
            var t = $('#TblThisProdutos').DataTable();
            var table_rows =
                '<tr id="tr' + data['id'] + '">' +
                '<td>' + (data['codigo'] == null ? '' : data['codigo']) + '</td>' +
                '<td>' + (data['nome'] == null ? '' : data['nome']) + '</td>' +
                '<td>' + (data['preco_loca'] == null ? '' : data['preco_loca']) + '</td>' +
                '<td>' + (data['qtd'] == null ? '' : data['qtd']) + '</td>' +
                '<td>' + (data['preco_repo'] == null ? '' : data['preco_repo']) + '</td>' +
                '<td>' + (data['preco_custo'] == null ? '' : data['preco_custo']) + '</td>' +
                '<td>' +
                '<a id="desvinculaOSit"' +
                ' onclick="deleteToOS(' + data['id'] + ')"' +
                ' class="btn btn-danger btn-xs glyphicon glyphicon-minus">' +
                '</a>' +
                '</td>' +
                '</tr>';
            // ->select('OSit.id','produtos.nome', 'produtos.codigo', 'OSit.preco_loca', 'produtos.preco_repo', 'produtos.preco_custo', 'OSit.qtd')
            t.rows.add($(table_rows)).draw();
            alertify.success('Vinculado');
            hidePleaseWait();
        }, error: function (e) {
            alertify.error('Erro ao vincular' + e.toString());
            hidePleaseWait();
        }
    });
}


function deleteToOS(id){
    showPleaseWait();
    console.log('desvincula ' + id);
    $.ajaxSetup({
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
    });
    $.ajax({
        type: "POST",
        url: '/ordem-servico/removeProduto/',
        dataType: "json",
        data: {
            id_OSit: id
        },
        success: function (data) {
            var t = $('#TblThisProdutos').DataTable();
            document.getElementById("tr" + id).classList.add('selected');
            t.row('.selected').remove().draw(false);
            alertify.success('Removido');
            hidePleaseWait();
        }, error: function (e) {
            alertify.error('Erro ao remover');
            hidePleaseWait()
        }
    });

}

// function openAddModal(produto, OS){
//     showAddProduto();
// }

function showAddProduto(produto, OS, preco_loca_it) {
    if ($("#produtoAddToOSModal").text() == '') {
        loadAddProduto();
    }

    $("#produtoAddToOSModal").modal("show");

    produto_id = produto;
    os_id = OS;
    preco_loca  = preco_loca_it;
    console.log(' selecionado ' +produto_id + ' ' + os_id + ' ' + preco_loca);

    document.getElementById('modal_preco_loca').value = parseFloat(preco_loca).toFixed(2);
    document.getElementById('modal_qtd').value = 1;
}

function hideAddProduto() {
    $("#produtoAddToOSModal").modal("hide");
}


// function buscaProduto(value) {
// console.log(value);
//     $.ajaxSetup({
//         headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
//     });
//     $.ajax({
//         type: "post",
//         url: '/cliente/gettotodos/',
//         dataType: "json",
//         data: {
//             filtro: value
//         },
//         success: function (data) {
//             var values;
//             if (data == '') {
//                 console.log("buscou vazio");
//                 document.getElementById("buscacontato").style.backgroundColor = "#ff5c33";
//             } else {
//                 console.log("buscou");
//                 values = $.map(data, function (item) {
//                     console.log(item.nome);
//                 });
//             }
//             // response(values);
//         }
//     });
// }