/*
 * Validao preenchiento da senha no reset
 * */
function valida() {
    var pw1 = document.getElementById('password1').value.toString();
    var pw2 = document.getElementById('password2').value.toString();

    if ((pw1 !== "" && pw2 !== "")) {
        if (pw1 === pw2) {
            document.getElementById("submit").disabled = false;
            document.getElementById('submit_btn').innerHTML = "";
        } else {
            document.getElementById("submit").disabled = true;
            document.getElementById('submit_btn').innerHTML = "Senha diferentes";
        }
    } else {
        document.getElementById("submit").disabled = true;
        document.getElementById('submit_btn').innerHTML = "";
    }
}

/*
 * cria modal para deleta registros
 * */
$(document).on('click', '#deletaConfirm', function (e) {
    var id = $(this).data('id');
    var route = $(this).data('route');
    var text = $(this).data('text');
    e.preventDefault();
    swal({
            title: text + id + " ?",
            text: "após deletar não há como voltar",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Excluir!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false,
            allowOutsideClick: true,
            allowEscapeKey: true,
            showLoaderOnConfirm: true
        },
        function (isConfirm) {
            if (isConfirm) {
                window.location.href = route;
            } else {
                swal("Cancelar", "Operação abortada", "error");
            }
        });
});

// function isPessoaFisica(element) {
//     console.log(element.checked);
//     if (element.checked) {
//         document.getElementById("pj").style.visibility = 'hidden';
//
//         document.getElementById("pf").style.visibility = 'visible';
//     } else {
//         document.getElementById("pj").style.visibility = 'visible';
//
//         document.getElementById("pf").style.visibility = 'hidden';
//     }
//
// }

function disable_if_readonly() {
    document.getElementById("readonly").disabled = true;
    var nodes = document.getElementById("readonly").getElementsByTagName('*');
    for (var i = 0; i < nodes.length; i++) {
        nodes[i].readOnly = true;
    }
}

function formatDecimal(element) {
    var v;
    v = parseFloat(element.value);
    element.value = parseFloat(Math.round(v * 100) / 100).toFixed(2);
}

function addEvent(element, evnt, funct){
    if (element.attachEvent)
        return element.attachEvent('on'+evnt, funct);
    else
        return element.addEventListener(evnt, funct, false);
}