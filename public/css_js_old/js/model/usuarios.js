/**
 * Created by Lucas on 09/08/2017.
 */
function userValidate() {
    var pw1 = document.getElementById('password1').value.toString();
    var pw2 = document.getElementById('password2').value.toString();

    if ((pw1!== "" && pw2!=="")){
        if(pw1===pw2) {
            document.getElementById('submit_btn').innerHTML = "";
            document.getElementById("submit").disable = false;
        } else {
            document.getElementById("submit").disable = true;
            document.getElementById('submit_btn').innerHTML = "Senha diferentes";
        }
    }else {
        document.getElementById('submit_btn').innerHTML = "Preencha senha iguais";
        document.getElementById("submit").disable = true;
    }
}