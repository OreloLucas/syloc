<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    ['middleware' => 'RedirectIfAuthenticated'],
    function () {
//        Route::get('/', ['as' => 'login', function () {
//            return view('login.index');
//        }]);
        Route::get('', ['as' => '', function () {
        }]);
        /* LOGIN */
        Route::get('login', ['as' => 'login', 'uses' => 'LoginController@login']);
//        Route::get('', ['as' => '', 'uses' => 'LoginController@login']);
    });

Route::get('/ordem-servico/imprime/', ['as' => 'os.imprime', 'uses' => 'OSController@imprime']);
Route::get('/ordem-servico/pdf/', ['as' => 'os.pdf', 'uses' => 'PDFController@pdf']);

/*Sem permissão*/
Route::post('/login', ['as' => 'login', 'uses' => 'LoginController@doLogin']);

Route::get('/logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);


Route::get('reset/{token?}', ['as' => 'reset', function ($token = null) {
    return view('login.reset', compact('token'));
}]);

Route::post('reset', ['as' => 'reset', 'uses' => 'UserController@reset']);
/*Sem permissão*/


/* LOGADO */
Route::group(['middleware' => 'UserAuthenticated'], function () {
    Route::get('/dashboard', ['as' => 'dashboard', function () {
        return view('dashboard');
    }]);

    /* CLIENTE */
    Route::get('/clientes', ['as' => 'clientes', 'uses' => 'ClienteController@lista']);
    Route::get('/cliente/novo', ['as' => 'cliente.adicionar', 'uses' => 'ClienteController@adicionar']);
    Route::get('/cliente/editar/{id?}', ['as' => 'cliente.editar', 'uses' => 'ClienteController@editar']);
    Route::get('/cliente/detalhe/{id?}', ['as' => 'cliente.detalhe', 'uses' => 'ClienteController@detalhe']);
    Route::post('/cliente/salvar', ['as' => 'cliente.salvar', 'uses' => 'ClienteController@salvar']);
    Route::post('/cliente/deleta/', ['as' => 'cliente.deleta', 'uses' => 'ClienteController@deleta']);

    Route::post('/cliente/vinculaContato', ['as' => 'cliente.vinculaContato', 'uses' => 'ClienteController@vinculaContato']);
    Route::post('/cliente/desvinculaContato', ['as' => 'cliente.desvinculaContato', 'uses' => 'ClienteController@desvinculaContato']);
    Route::post('/cliente/gettotodos/', ['as' => 'cliente.gettotods', 'uses' => 'ClienteController@gettotods']);

    /* FORNECEDOR */
    Route::get('/fornecedores', ['as' => 'fornecedores', 'uses' => 'FornecedorController@lista']);
    Route::get('/fornecedor/novo', ['as' => 'fornecedor.adicionar', 'uses' => 'FornecedorController@adicionar']);
    Route::get('/fornecedor/editar/{id?}', ['as' => 'fornecedor.editar', 'uses' => 'FornecedorController@editar']);
    Route::get('/fornecedor/detalhe/{id?}', ['as' => 'fornecedor.detalhe', 'uses' => 'FornecedorController@detalhe']);
    Route::post('/fornecedor/salvar', ['as' => 'fornecedor.salvar', 'uses' => 'FornecedorController@salvar']);
    Route::post('/fornecedor/deleta/', ['as' => 'fornecedor.deleta', 'uses' => 'FornecedorController@deleta']);

    Route::post('/fornecedor/vinculaContato', ['as' => 'fornecedor.vinculaContato', 'uses' => 'FornecedorController@vinculaContato']);
    Route::post('/fornecedor/desvinculaContato', ['as' => 'fornecedor.desvinculaContato', 'uses' => 'FornecedorController@desvinculaContato']);
    Route::post('/fornecedor/gettotodos/', ['as' => 'fornecedor.gettotodos', 'uses' => 'FornecedorController@gettotods']);

    /* PRODUTO */
    Route::get('/produtos', ['as' => 'produtos', 'uses' => 'ProdutoController@lista']);
    Route::get('/produto/novo', ['as' => 'produto.adicionar', 'uses' => 'ProdutoController@adicionar']);
    Route::get('/produto/editar/{id?}', ['as' => 'produto.editar', 'uses' => 'ProdutoController@editar']);
    Route::get('/produto/detalhe/{id?}', ['as' => 'produto.detalhe', 'uses' => 'ProdutoController@detalhe']);
    Route::post('/produto/salvar', ['as' => 'produto.salvar', 'uses' => 'ProdutoController@salvar']);
    Route::post('/produto/deleta/', ['as' => 'produto.deleta', 'uses' => 'ProdutoController@deleta']);
    Route::post('/produto/buscaProduto/', ['as' => 'produto.buscaProduto', 'uses' => 'ProdutoController@buscaProduto']);


    /* LOCAL ENTREGA */
    Route::get('/local-entrega', ['as' => 'local-entrega', 'uses' => 'LocalEntregaController@lista']);
    Route::get('/local-entrega/novo', ['as' => 'local-entrega.adicionar', 'uses' => 'LocalEntregaController@adicionar']);
    Route::get('/local-entrega/editar/{id?}', ['as' => 'local-entrega.editar', 'uses' => 'LocalEntregaController@editar']);
    Route::get('/local-entrega/detalhe/{id?}', ['as' => 'local-entrega.detalhe', 'uses' => 'LocalEntregaController@detalhe']);
    Route::post('/local-entrega/salvar', ['as' => 'local-entrega.salvar', 'uses' => 'LocalEntregaController@salvar']);
    Route::post('/local-entrega/deleta/', ['as' => 'local-entrega.deleta', 'uses' => 'LocalEntregaController@deleta']);

    /*CONTATO*/
    Route::get('/contatos', ['as' => 'contatos', 'uses' => 'ContatoController@lista']);
    Route::get('/contato/novo/{id_cliente?}', ['as' => 'contato.adicionar', 'uses' => 'ContatoController@adicionar']);
    Route::get('/contato/editar/{id?}', ['as' => 'contato.editar', 'uses' => 'ContatoController@editar']);
    Route::get('/contato/detalhe/{id?}', ['as' => 'contato.detalhe', 'uses' => 'ContatoController@detalhe']);
    Route::post('/contato/salvar', ['as' => 'contato.salvar', 'uses' => 'ContatoController@salvar']);
    Route::post('/contato/deleta/', ['as' => 'contato.deleta', 'uses' => 'ContatoController@deleta']);

//    Route::post('/contato/teste',['as'=>'contato.teste', 'uses' => 'ContatoController@index']); //'ContatoController@index');
//
//
//    /*CONTRATOS*/
//    Route::get('/contratos',['as'=>'contratos', function () {
//        return "";
//    }]);

    /*os*/
    Route::get('/ordem-servico', ['as' => 'os', 'uses' => 'OSController@lista']);
    Route::get('/ordem-servico/novo', ['as' => 'os.adicionar', 'uses' => 'OSController@adicionar']);
    Route::get('/ordem-servico/editar/{id?}', ['as' => 'os.editar', 'uses' => 'OSController@editar']);
    Route::get('/ordem-servico/detalhe/{id?}', ['as' => 'os.detalhe', 'uses' => 'OSController@detalhe']);
    Route::post('/ordem-servico/salvar', ['as' => 'os.salvar', 'uses' => 'OSController@salvar']);
    Route::post('/ordem-servico/deleta/', ['as' => 'os.deleta', 'uses' => 'OSController@deleta']);
    Route::get('/ordem-servico/produtos/{id?}', ['as' => 'os.produtos', 'uses' => 'OSController@produtos']);
    Route::post('/ordem-servico/addProduto/', ['as' => 'os.addProduto', 'uses' => 'OSController@addProduto']);
//    Route::get('/ordem-servico/addProduto/{produto}/{os}', ['as' => 'os.addProduto', 'uses' => 'OSController@addProduto']);
    Route::post('/ordem-servico/removeProduto/', ['as' => 'os.removeProduto', 'uses' => 'OSController@removeProduto']);

    /* USUARIO */
    Route::get('/usuarios', ['as' => 'usuarios', 'uses' => 'UserController@lista']);
    Route::get('/usuario/novo', ['as' => 'usuario.adicionar', 'uses' => 'UserController@adicionar']);
    Route::get('/usuario/editar/{id?}', ['as' => 'usuario.editar', 'uses' => 'UserController@editar']);
    Route::get('/usuario/detalhe/{id?}', ['as' => 'usuario.detalhe', 'uses' => 'UserController@detalhe']);
    Route::post('/usuario/salvar', ['as' => 'usuario.salvar', 'uses' => 'UserController@salvar']);
    Route::post('/usuario/deleta/', ['as' => 'usuario.deleta', 'uses' => 'UserController@deleta']);

    /* SOBRE */
    Route::get('/sobre', ['as' => 'sobre', 'uses' => 'EmpresaController@lista']);

    /* MODALS */
    Route::get('/modal/contatoModal/{model?}', ['as' => 'modal.contatoModal', 'uses' => 'ModalController@contato']);

    Route::get('/modal/sLoadModal/', ['as' => 'modal.sLoadModal', function () {
        return view('modal.sLoadModal');
    }]);

    Route::get('/modal/produtoAddToOSModal/', ['as' => 'modal.produtoAddToOSModal', function () {
        return view('modal.produtoAddToOSModal');
    }]);

    Route::get('/modal/LoadModal/', ['as' => 'modal.loadModal', function () {
        return view('modal.loadModal');
    }]);


    /**/
    Route::post('/utils/clientes', ['as' => 'utils.clientes', 'uses' => 'UtilsController@clientes']);
    Route::post('/utils/localentrega', ['as' => 'utils.clientes', 'uses' => 'UtilsController@localentrega']);

});

