<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OSEntrega extends Model
{
    protected $fillable = [
        'idEntrega', 'id_empresa','id_local_entrega', 'nome_entrega', 'endereco', 'numero', 'bairro', 'cidade' ,'UF',
    ];

    protected $table = 'osentrega';
}
