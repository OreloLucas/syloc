<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fornecedores extends Model
{
    protected $fillable = [
        'id_empresa','razao', 'fantasia', 'cnpj', 'cpf' ,'ierg','fone','email', 'is_pessoa_fisica',
    ];

    public function contatos() {
        return $this->hasMany('App\Contatos');
    }
    public function CountContatos() {
        return $this->hasMany('App\Contatos')->count();
    }
}
