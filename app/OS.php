<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Os extends Model
{
    protected $table = 'OS';

    protected $fillable = [
        'id','id_empresa', 'id_cliente', 'id_osentrega',

        /* HORARIO */
        'dtevento','dtentrega','dtrecolhimento',

        /* ENTREGA */
        'id_local_entrega',
//        'entrega_nome', 'entrega_endereco', 'entrega_numero',
//        'entrega_bairro', 'entrega_cidade' ,'entrega_UF', 'entrega_referencia',

        'obscliente', 'obsinterna',
    ];

    public function produtos($id) { //produtos da OS
        return OSit::where([
            ['OSit.id_empresa', '=', auth()->user()['id_empresa']],
            ['produtos.id_empresa', '=', auth()->user()['id_empresa']],
            ['OSit.os_id', '=', $id],
        ])
            ->join('produtos' , 'produtos.id', '=', 'OSit.id_prod')
            ->select('OSit.id','produtos.nome', 'produtos.codigo', 'OSit.preco_loca', 'produtos.preco_repo', 'produtos.preco_custo', 'OSit.qtd')
            ->get();
    }

//    public function CountContatos() {
//        return $this->hasMany('App\Contatos')->count();
//    }


}
