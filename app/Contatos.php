<?php

namespace App;

use App\Http\Requests\ContatoRequest;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\ModelController;

class Contatos extends Model
{
//    public function __construct(array $attributes = [])
//    {
//        parent::__construct($attributes);
////        $model['nome'] = $attributes->get('nome');
////        $model['fornecedores_id'] = 0; // = $request->get('fornecedores_id');
////        $model['clientes_id'] = $attributes->get('cliente_id');
////        $model['rg'] = $attributes->get('rg');
////        $model['cpf'] = $attributes->get('cpf');
////        $model['email'] = $attributes->get('email');
////        $model['fone1'] = $attributes->get('fone1');
////        $model['fone2'] = $attributes->get('fone2');
////        $model['fone3'] = $attributes->get('fone3');
//    }

    public static function vinculaContato(ContatoRequest $request, $param)
    {
        try {
            $id = $request->get('id');
            if ($id > 0) { //contato já existe
                $contato = Contatos::find($id);
                if ($request->get('fornecedores_id') <> 0)
                    $contato['fornecedores_id'] = $request->get('fornecedores_id');

                if ($request->get('cliente_id') <> 0)
                    $contato['clientes_id'] = $request->get('cliente_id');

                $contato->save();
                return $contato;
            } else { //contato não existe
                $model[] = [];
                $model['nome'] = $request->get('nome');
                $model['clientes_id'] = $request->get('cliente_id');
                $model['fornecedores_id'] = $request->get('fornecedores_id');
                $model['rg'] = $request->get('rg');
                $model['cpf'] = $request->get('cpf');
                $model['email'] = $request->get('email');
                $model['fone1'] = $request->get('fone1');
                $model['fone2'] = $request->get('fone2');
                $model['fone3'] = $request->get('fone3');
                return ModelController::salvarModelAJAX($model, $param);
            }
        } catch (\Exception $e) {
            return ['success' => $e];
        }
    }

    protected $fillable = [
        'fornecedores_id', 'clientes_id',
        'fornecedor', 'cliente',
        'id_empresa', 'nome', 'cpf', 'rg', 'fone1', 'fone2', 'fone3', 'email'
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Clientes');
    }

    public function fornecedores()
    {
        return $this->belongsTo('App\Fornecedores');
    }
}
