<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\ModelController;
use Illuminate\Http\Request;

class OSit extends Model
{
    //

    protected $table = 'OSit';

    protected $fillable = ['id', 'os_id', 'id_prod', 'qtd', 'id_empresa', 'codigo', 'nome', 'fornecedores_id',
        'preco_loca', 'preco_repo', 'preco_custo', 'descricao', 'descricao'];

    public static function vinculaProduto(Request $request, $param)
    {
        try {
            $id = $request->get('id_prod');
            if ($id > 0) { //produto já existe
                $produto = Produtos::find($id);

                $model[] = [];
//                $osIT['id_empresa'] = $produto['id_empresa'];
                $model['id_prod'] = $produto['id'];

                $model['os_id'] = $request->get('id_os');
//                $model['os_id'] = $os;

                $model['qtd'] = $request->get('qtd');
//                $model['qtd'] = 1;

                $model['preco_loca'] = $request->get('preco_loca');
//                $model['preco_loca'] = $produto['preco_loca'];


//                $osIT['qtd'] = $request->get('qtd');

                $modelR = ModelController::salvarModelAJAX($model, $param);

                //if (isset($modelR['id']) and $modelR['id'] > 0){
//                    $select = OSit::where([
//                        ['OSit.id_empresa', '=', auth()->user()['id_empresa']],
//                        ['produtos.id_empresa', '=', auth()->user()['id_empresa']],
//                        ['OSit.id', '=', $modelR['id']],
//                    ])
//                        ->join('produtos' , 'produtos.id', '=', 'OSit.id_prod')
//                        ->select('OSit.id','produtos.nome', 'produtos.codigo', 'OSit.preco_loca', 'produtos.preco_repo', 'produtos.preco_custo', 'OSit.qtd')
//                        ->get();
////                    $modelR->id = $select->id;

                //}
                //$modelR->id = 1;
                $modelR['codigo'] = $produto['codigo'];
                $modelR['nome'] = $produto['nome'];
                $modelR['preco_repo'] = $produto['preco_repo'];
                $modelR['preco_custo'] = $produto['preco_custo'];
                return $modelR;
//                return $select;
            }
        } catch (\Exception $e) {
            return ['error aaaa' => $e];
        }
    }

    public static function desvinculaProduto(Request $request)
    {
        try {
            $osIT = OSit::find($request->get('id_OSit'));
            $osIT->delete();
            return ['success' => 'true'];
        } catch (\Exception $e) {
            return ['success' => $e];
        }
    }

    public static function produto($id)
    {
//        return
    }
}
