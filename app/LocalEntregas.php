<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocalEntregas extends Model
{
    protected $fillable = [
        'id_empresa','id_local_entrega', 'id_os' , 'nome', 'endereco', 'numero', 'bairro', 'cidade' ,'UF',
    ];
}
