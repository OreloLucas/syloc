<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\LocalEntregas;
use App\Produtos;
use Illuminate\Http\Request;
use App\os;
use App\OSit;
use App\Empresa;
use App\Http\Requests\ProdutoRequest;
use App\Contatos;
use App\Http\Requests\ClienteRequest;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ContatoRequest;

class OSController extends Controller
{
    private $param;

    public function __construct()
    {
        $this->param = [
            'formName' => "OS",
            'formHeader' => "Ordem de Serviço",
            'form' => '_models.os._form',
            'routes' => [
                "lista" => "os",
                "novo" => "os.adicionar",
                "detalhe" => "os.detalhe",
                "editar" => "os.editar",
                "excluir" => "os.deleta",
                "deletaAJAX" => "/ordem-servico/deleta/",
                "salvar" => "os.salvar",
                "voltar" => "os",
                "OSit" => "_models.OS.produtos"],
            'excluir' => "Deseja deletar a os ? ",
            'clazz' => \App\OS::class,
            'acoes' => ['detalhe', 'editar', 'excluir'],
        ];
    }

    public function lista()
    {
        $param = $this->param;
        $header = [
            1 => 'Razao',
            2 => 'Local Entrega',
            99 => 'acoes',
        ];

        $body = [
            1 => "razao",
            2 => "entrega_nome",
        ];

        $lista = OS::where([
            ['OS.id_empresa', '=', auth()->user()['id_empresa']],
            ['clientes.id_empresa', '=', auth()->user()['id_empresa']],
        ])
            ->join('clientes', 'OS.id_cliente', '=', 'clientes.id')
            ->select('OS.id', 'clientes.razao', 'OS.entrega_nome')
            ->get();

        return ModelController::listarLista($lista, $param, $header, $body);
    }

    public function adicionar()
    {
        $param = $this->param;
        return ModelController::adicionar($param);
    }

    public function editar($id = null)
    {
        $param = $this->param;
        $model = OS::find($id);
        $this->formatModel($model);
        $result = ModelController::editarModel($model, $param, 'Fornecedor não encontrado');
        $result['busca_produto'] = Produtos::where([['id_empresa', '=', auth()->user()['id_empresa']]])->get();
        $result['this_produto'] = $model->produtos(1);
        return $result;
//        return $result;
    }

    public function detalhe($id = null)
    {
        $param = $this->param;
        $model = OS::find($id);
        $this->formatModel($model);
        $result = ModelController::detalheModel($model, $param, 'Fornecedor não encontrado');

        return $result;
    }

    private function formatModel($model)
    {
        $model['horaEvento'] = UtilsController::formatTime($model['dtevento']);
        $model['horaEntrega'] = UtilsController::formatTime($model['dtentrega']);
        $model['horaRecolhimento'] = UtilsController::formatTime($model['dtrecolhimento']);

        $model['dtevento'] = UtilsController::formatDate($model['dtevento']);
        $model['dtentrega'] = UtilsController::formatDate($model['dtentrega']);
        $model['dtrecolhimento'] = UtilsController::formatDate($model['dtrecolhimento']);

        $cliente = Clientes::find($model['id_cliente']);
        $model['razao'] = $cliente['razao'];
        $model['cnpj'] = $cliente['cnpj'];

        $entrega = LocalEntregas::find($model['id_local_entrega']);

        $model['entrega'] = $entrega['nome'] . '&#13;&#10;' .
            $entrega['endereco'] . ', ' . $entrega['numero'] . '&#13;&#10;' .
            $entrega['bairro'] . ' - ' . $entrega['cidade'] . ' - (' . $entrega['UF'] . ')';
//        dd($model['entrega']);

//        ui.item.nome_entrega + '\n' +
//            ui.item.endereco + ', ' +  ui.item.numero +'\n' + // ' - ' + ui.item.CEP
//            ui.item.bairro + ' - ' + ui.item.cidade + ' - (' + ui.item.UF.toUpperCase() + ')';
        return $model;
    }

    public function salvar(Request $request)
    {
        $param = $this->param;
        $model = $request->all();

        $model['dtevento'] = UtilsController::formatDateTime('Y-m-d H:i:s', $model['dtEvento'], $model['horaEvento']);
        $model['dtentrega'] = UtilsController::formatDateTime('Y-m-d H:i:s', $model['dtEntrega'], $model['horaEntrega']);
        $model['dtrecolhimento'] = UtilsController::formatDateTime('Y-m-d H:i:s', $model['dtRecolhimento'], $model['horaRecolhimento']);

        return ModelController::salvarModel($model, $param);
    }

    public function deleta(Request $request)
    {
        try {
            $id = $request->get('id');
            $model = OS::find($id);
            return ModelController::deletaModelAJAX($model);
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }

    public function produtos($id = null)
    {
        $param = $this->param;
        $param['route'] = $this->param['routes']['OSit'];
        return ModelController::callView($param);
    }

    public function addProduto(Request $request)
    {
        $param = $this->param;
        $param['clazz'] = \App\OSit::class;
        return OSit::vinculaProduto($request,$param);
    }


    public function removeProduto(Request $request){
        return OSit::desvinculaProduto($request);
    }

    public function imprime()
    {
        $empresa = Empresa::find(1);

        $os = OS::find(1);

        $cliente = Clientes::find($os['id_cliente']);
        $entrega = LocalEntregas::find($os['id_local_entrega']);
        $data = [
            'empresa' => $empresa,
            'os' => $os,
            'cliente' => $cliente,
            'entrega' => $entrega];

//        return view('_models.OS.entrega',compact('os','cliente','entrega','empresa'));
        return PDFController::showPDF('_models.OS.entrega', $data, 'teste');
    }

}
