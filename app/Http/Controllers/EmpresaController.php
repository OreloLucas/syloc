<?php

namespace App\Http\Controllers;


use App\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\http\Requests;

class EmpresaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function lista(){
        $sobre = Empresa::find(auth()->user()['id_empresa']);
        return view('sobre',compact('sobre'));
    }
}
