<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\PDF;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use JonnyW\PhantomJs\Client;
use Barryvdh\DomPDF\Facade;
use Illuminate\Support\Facades\DB;

class PDFController extends Controller
{
    /*
    * view - caminho da view do relatório
    * data - compact() com váriaveis para view
    * file -  nome do arquivo
    */

    public static function downloadPDF($view, $data, $file = 'report')
    {
        return PDFController::loadView($view, $data)->download($file . '.pdf');
    }

    public static function showPDF($view, $data)
    {
        return PDFController::loadView($view, $data)->stream('report.pdf');
    }

    private static function loadView($view, $data)
    {
        $pdf = App::make('dompdf.wrapper');
//        $pdf->setOptions([
//            'isHtml5ParserEnabled' => true,
//            'isRemoteEnabled' => true,
//            'verify_peer' => FALSE,
//            'verify_peer_name' => FALSE,
//            'allow_self_signed'=> TRUE,
//            'allow_url_fopen' => TRUE,]);
        $pdf->loadView($view, $data);
        return $pdf;
    }

//    private $client;
//    public function __construct()
//    {
//        $this->client = Client::getInstance();
//        $this->client->getEngine()->setPath('bin\phantomjs.exe');
//    }
//    public function geraPDF_POST($url, $arquivo, $orientacao = "portland", $param)
//    {
////        $this->client = Client::getInstance();
////        $this->client->getEngine()->setPath('bin\phantomjs.exe');
////        $this->client->getEngine()->debug(true);
//        $request = $this->client->getMessageFactory()->createPdfRequest($url, 'POST');
//        $request->addHeader('X-CSRF-TOKEN', csrf_token());
//        $request->setOutputFile('storage/' . $arquivo . '.pdf');
//        $request->setFormat('A4');
//        $request->setOrientation($orientacao);
//        $request->setMargin('1cm');
//        $request->setRequestData($param);
//
//        $response = $this->client->getMessageFactory()->createResponse();
//
//        $this->client->send($request, $response);
//
////        dd($this->client->getLog());
//    }
//
//    /*
//     * url        - URL do relatório
//     * arquivo    - nome do arquivo
//     * orientacao - 'landscape' ou portland
//     */
//    public function geraPDF_GET($url, $arquivo, $orientacao = "portland")
//    {
////        $this->client = Client::getInstance();
////        $this->client->getEngine()->setPath('bin\phantomjs.exe');
//        $this->client->getEngine()->debug(true);
//        $request = $this->client->getMessageFactory()->createPdfRequest($url);
//        //$this->client->getEngine()->addOption(' --log-level=[debug|info|warning|error] ');
//        $request->addHeader('X-CSRF-Token', csrf_token());
//        $request->setOutputFile('storage/' . $arquivo . '.pdf');
//        $request->setFormat('A4');
//        $request->setOrientation($orientacao);
//        $request->setMargin('1cm');
//       // dd($request);
//        $response = $this->client->getMessageFactory()->createResponse();
//
//        $this->client->send($request, $response);
////        dd($this->client->getLog());
////        dd($response->getConsole());
//        dd($this->client->getEngine()->getLog());
//    }
//
//    public function doDownload($arquivo)
//    {
//        //ajustar
//        //return response()->download(public_path().'/storage/'.$arquivo.'.pdf')->deleteFileAfterSend(true);
//    }
//
//    public function show($arquivo)
//    {
//        return redirect('/storage/' . $arquivo . '.pdf');
//
//    }
}
