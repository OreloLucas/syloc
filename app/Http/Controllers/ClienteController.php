<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\Contatos;
use App\Http\Requests\ClienteRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ContatoRequest;

class ClienteController extends Controller
{
    private $param;

    public function __construct()
    {
        $this->param = [
            'formName' => "Clientes",
            'formHeader' => "cliente",
            'form' => '_models.cliente._form',
            'routes' => [
                "lista" => "clientes",
                "novo" => "cliente.adicionar",
                "detalhe" => "cliente.detalhe",
                "editar" => "cliente.editar",
                "excluir" => "cliente.deleta",
                "deletaAJAX" => "/cliente/deleta/",
                "salvar" => "cliente.salvar",
                "voltar" => "clientes",],
            'excluir' => "Deseja deletar o cliente ",
            'clazz' => \App\Clientes::class,
            'acoes' => ['detalhe', 'editar', 'excluir'],
        ];
    }

    public function lista()
    {
        $param = $this->param;
        $header = [
            // 0 => "#",
            1 => "razao",
            2 => "fantasia",
            3 => "cnpj/cpf",
            4 => 'acoes',

        ];

        $body = [
            //0=> 'id',
            1 => "razao",
            2 => "fantasia",
            3 => "cnpj|cpf",
        ];

        return ModelController::listar($param, $header, $body);
    }

    public function adicionar()
    {
        $param = $this->param;
        return ModelController::adicionar($param);
    }

    public function editar($id = null)
    {
        $param = $this->param;
        $model = Clientes::find($id);
        $result = ModelController::editarModel($model, $param, 'Cliente não encontrado');
        $result['this_contatos'] = $model->contatos;
        return $result;

    }

    public function detalhe($id = null)
    {
        $param = $this->param;
        $model = Clientes::find($id);
        $result = ModelController::detalheModel($model, $param, 'Cliente não encontrado');
        $result['this_contatos'] = $model->contatos;
//        $result['contatos'] = $this->getTodosContatos();
        return $result;
    }

    public function salvar(ClienteRequest $request)
    {
        $param = $this->param;
        $model = $request->all();

        if (!isset($model['is_pessoa_fisica'])) {
            $model['is_pessoa_fisica'] = 0;
            $model['cpf'] = '';
        } else {
            $model['cnpj'] = '';
        }

        return ModelController::salvarModel($model, $param);

    }

    public function deleta(Request $request)//($id = null)
    {
        //$param = $this->param;
        //return app('App\Http\Controllers\ModelController')->deleta($id, $param);

        try {
            $id = $request->get('id');
            $model = Clientes::find($id);

            return ModelController::deletaModelAJAX($model);
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }

    public function vinculaContato(ContatoRequest $request)//($id_cliente,$id_contato)
    {
        $param = $this->param;
        $param['clazz'] = \App\Contatos::class;
        return Contatos::vinculaContato($request,$param);
    }

    public function desvinculaContato(Request $request)//($id_contato)
    {
        $id_contato = $request->get('contato_id');
        try {
            $contato = Contatos::find($id_contato);
            $contato->clientes_id = 0;
            $contato->save();
            return ['success' => true];
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }

    public function gettotods(Request $request)//($id_contato)
    {
        //sleep(5);
        $filtro = $request->get('filtro');
        try {
            return $this->getTodosContatos($filtro);
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }

    public function getTodosContatos($filtro = '', $cliente_id = 0)
    {
        $connection = config('database.default');

        $driver = config("database.connections.{$connection}.driver");

        if ($driver == 'mysql') {
            return Contatos::where([
                ['id_empresa', '=', auth()->user()['id_empresa']], // busca da mesma empresa
                ['nome', 'like', '' . $filtro . '%'], //busca baseado no filtro
                ['clientes_id', '=', 0] //busca que não tem cliente
            ])->orderBy('nome', 'asc')->get();
        } else if ($driver == 'pgsql') {
            return Contatos::where([
                ['id_empresa', '=', auth()->user()['id_empresa']], // busca da mesma empresa
                ['nome', 'ilike', '' . $filtro . '%'], //busca baseado no filtro
                ['clientes_id', '=', 0]//busca que não tem cliente
            ])->orderBy('nome', 'asc')->get();
        }
    }
}
