<?php

namespace App\Http\Controllers;

use App\Http\Requests\LocalEntregaRequest;
use App\LocalEntregas;
use Illuminate\Http\Request;

class LocalEntregaController extends Controller
{
    private $param;

    public function __construct()
    {
        $this->param = [
            'formName' => "Local de entrega",
            'formHeader' => "local de entrega",
            'form' => '_models.local_entrega._form',
            'routes' => [
                "lista"     => "local-entrega",
                "novo"      => "local-entrega.adicionar",
                "detalhe"   => "local-entrega.detalhe",
                "editar"    => "local-entrega.editar",
                "excluir"   => "local-entrega.deleta",
                "deletaAJAX" => "/local-entrega/deleta/",
                "salvar"    => "local-entrega.salvar",
                "voltar"    => "local-entrega", ] ,
            'excluir' => "Deseja deletar o fornecedor ",
            'clazz' => \App\LocalEntregas::class,
            'acoes' => ['detalhe','editar','excluir'],
        ];
    }

    public function lista()
    {
        $param = $this->param;
        $header = [
            0 => "#",
            1 => "nome",
            2 => "endereco",
            3 => "numero",
            4 => 'acoes',
        ];

        $body = [
            0 => "id",
            1 => "nome",
            2 => "endereco",
            3 => "numero",
        ];
        $paginate = 10;
        return ModelController::listar($param,$header,$body);
    }

    public function adicionar(){
        $param = $this->param;
        return ModelController::adicionar($param);
    }

    public function editar($id = null){
        $param = $this->param;
        return ModelController::editar($id,$param,'Fornecedor não encontrado');
    }

    public function detalhe($id = null){
        $param = $this->param;
//        return ModelController::detalhe($id,$param,'Fornecedor não encontrado');

    }

    public function salvar(LocalEntregaRequest $request){
        $param = $this->param;
        return ModelController::salvar($request,$param);
    }

    public function deleta(Request $request){
        try {
            $id = $request->get('id');
            $model = LocalEntregas::find($id);
            return ModelController::deletaModelAJAX($model);
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }
}
