<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clientes;
use App\Contatos;
use App\LocalEntregas;

class UtilsController extends Controller
{
    private $driver;

    public function __construct()
    {
        $connection = config('database.default');

        $this->driver = config("database.connections.{$connection}.driver");
    }

    public function clientes(Request $request)
    {
        $filtro = $request->get('filtro');
        try {
            if ($this->driver == 'mysql') {
                return Clientes::where([
                    ['id_empresa', '=', auth()->user()['id_empresa']],  // busca da mesma empresa
                    ['razao', 'like', '' . $filtro . '%'],               //busca que não tem fornecedor
                ])->orderBy('razao', 'asc')->get();
            } else if ($this->driver == 'pgsql') {
                return Clientes::where([
                    ['id_empresa', '=', auth()->user()['id_empresa']],  // busca da mesma empresa
                    ['razao', 'ilike', '' . $filtro . '%']            //busca baseado no filtro
                ])->orderBy('razao', 'asc')->get();
            }
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }

    public function localentrega(Request $request)
    {
        $filtro = $request->get('filtro');
        try {
            if ($this->driver == 'mysql') {
                return LocalEntregas::where([
                    ['id_empresa', '=', auth()->user()['id_empresa']],  // busca da mesma empresa
                    ['nome', 'like', '' . $filtro . '%'],               //busca que não tem fornecedor
                ])->orderBy('razao', 'asc')->get();
            } else if ($this->driver == 'pgsql') {
                return LocalEntregas::where([
                    ['id_empresa', '=', auth()->user()['id_empresa']],  // busca da mesma empresa
                    ['nome', 'ilike', '' . $filtro . '%']            //busca baseado no filtro
                ])->orderBy('nome', 'asc')->get();
            }
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }

    public function contatos(Request $request)
    {
        $filtro = $request->get('filtro');
        try {
            if ($this->driver == 'mysql') {
                return Contatos::where([
                    ['id_empresa', '=', auth()->user()['id_empresa']],  // busca da mesma empresa
                    ['nome', 'like', '' . $filtro . '%'],               //busca baseado no filtro
                    ['clientes_id', '=', 0]                             //busca que não tem cliente
//                ['fornecedores_id', '=', 0]                           //busca que não tem fornecedor
                ])->orderBy('nome', 'asc')->get();
            } else if ($this->driver == 'pgsql') {
                return Contatos::where([
                    ['id_empresa', '=', auth()->user()['id_empresa']],  // busca da mesma empresa
                    ['nome', 'ilike', '' . $filtro . '%'],              //busca baseado no filtro
                    ['clientes_id', '=', 0]                             //busca que não tem cliente
//                ['fornecedores_id', '=', 0]                           //busca que não tem fornecedor
                ])->orderBy('nome', 'asc')->get();
            }
        } catch (\Exception $e) {
            return ['success' => false];
        }


    }

    public static function formatDateTime($format, $date, $time = '')
    {
        if ($time == null) {
            $time = '00:00:59';
        }
        if ($date == null) {
            $date = '01/01/1969';
        }

        $date = str_replace('/', '-', $date);

        return date($format, strtotime($date . ' ' . $time));
    }

    public static function formatTimeStamp($format, $timeStamp)
    {
        if ($timeStamp == null) {
            $timeStamp = '01/01/1969';
        }

        $timeStamp = date($format, strtotime($timeStamp));

        return $timeStamp;
    }

    public static function formatTime($originalTimeStamp)
    {
        $time = UtilsController::formatTimeStamp('H:i:s', $originalTimeStamp);
        if ((int)(substr($time, -2)) == 59) {
            $time = '';
        } else {
            $time = substr($time, 0, 5);
        }
        return $time;
    }

    public static function formatDate($originalTimeStamp)
    {
        $date = UtilsController::formatTimeStamp('d/m/Y', $originalTimeStamp);
        if ((int)(substr($date, -4)) < 2000) {
            return '';
        } else {
            return $date;
        }
    }
}
