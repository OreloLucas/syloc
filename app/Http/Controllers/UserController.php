<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\Http\Requests\UserRequest;
use App\PasswordResets;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Http\Controllers\Controller;


class UserController extends Controller
{
    private $param;

    public function __construct()
    {
        $this->param = [
            'formName' => "Usuarios",
            'formHeader' => "usuario",
            'form' => '_models.usuario._form',
            'routes' => [
                "lista" => "usuarios",
                "novo" => "usuario.adicionar",
                "detalhe" => "usuario.detalhe",
                "editar" => "usuario.editar",
                "excluir" => "usuario.deleta",
                "deletaAJAX" => "/usuario/deleta/",
                "salvar" => "usuario.salvar",
                "voltar" => "usuarios",],
            'excluir' => "Deseja deletar o usuario ",
            'clazz' => \App\User::class,
            'validate' => [
                'email' => 'required|unique|max:255',
                'nome' => 'required'
            ],
            'formValidate' => "userValidate()",
            'acoes' => ['detalhe', 'editar', 'excluir'],
        ];
    }

    public function lista()
    {
        $param = $this->param;
        $header = [
            //0 => "#",
            1 => "nome",
            2 => "sobrenome",
            3 => "email",
            4 => "cargo",
            5 => 'acoes',
        ];

        $body = [
            //0 => 'id',
            1 => "name",
            2 => "sobrenome",
            3 => "email",
            4 => "cargo",
        ];

        $paginate = 10;
        return ModelController::listar($param, $header, $body);
    }

    public function adicionar()
    {
        $param = $this->param;
        return ModelController::adicionar($param);
    }

    public function editar($id = null)
    {
        $param = $this->param;
        return ModelController::editar($id, $param, 'Usuário não encontrado');
    }

    public function detalhe($id = null)
    {
        $param = $this->param;
        return ModelController::detalhe($id, $param, 'Usuário não encontrado');
    }

    public function salvar(UserRequest $request)
    {
        $param = $this->param;
        $model = $request->all();

        if (isset($model['password1']) && isset($model['password2'])) {
            if ($model['password1'] == $model['password2']) {
                $model['password'] = bcrypt($model['password1']);
                return ModelController::salvarModel($model, $param);
            } else {
//                \Session::flash('mensagem',['msg'=>'erro ao criar cadastro','class' => 'alert alert-danger']);
                $this->errors = "senha diferentes";
                return redirect()->route('usuario.adicionar', compact('model'));
            }
        } else {
            return ModelController::salvar($request, $param);
        }
    }

    public function deleta($id = null)
    {
        $param = $this->param;

        if (auth()->id() == $id) {
            \Session::flash('mensagem', ['msg' => 'Usuário logado não pode deletar o proprio registro', 'class' => 'alert alert-danger']);
            return redirect()->route($param['routes']['lista']);
        }

        return ModelController::deleta($id, $param);
    }

    public function reset(Request $request)
    {
        $dados = $request->all();
        if (array_key_exists('email', $dados)) {
            $pr = DB::table('password_resets')->where('email', '=', $dados['email'])->get();
            if (count($pr) >= 1) {
                //envia email
                \Session::flash('mensagem', ['msg' => 'email de redefinição de senha enviado com sucesso', 'class' => 'alert alert-success']);

            } else {
                \Session::flash('mensagem', ['msg' => 'email não localizado', 'class' => 'alert alert-danger']);
            }
            return redirect()->route('reset');
        } else if (array_key_exists('password1', $dados) && array_key_exists('password2', $dados) && array_key_exists('token', $dados)) {
            if ($dados['password1'] === $dados['password2']) {
                $pr = DB::table('password_resets')->where('token', '=', $dados['token'])->get();
                dd($pr);
                $user = DB::table('user')->where('email', '=', $pr['email'])->get();
//                NrqnTvCqNVDol086cJSWsIPmv
            }
            \Session::flash('mensagem', ['msg' => 'senhas diferentes', 'class' => 'alert alert-danger']);
            return route('reset/' . $dados['token']);
        }
    }

}