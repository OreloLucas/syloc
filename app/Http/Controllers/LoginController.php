<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    public function login()
    {
        return view('login.index');
    }

    public function doLogin(Request $request)
    {
        $dados = $request->all();
        if ((auth()->attempt(['name' => $dados['login'], 'password' => $dados['password']])) ||
            (auth()->attempt(['email' => $dados['login'], 'password' => $dados['password']]))) {
            return redirect()->route('dashboard');
        }
        \Session::flash('mensagem', ['msg' => 'Senha ou usuário incorreto', 'class' => 'alert alert-danger']);
        return redirect()->route('login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
