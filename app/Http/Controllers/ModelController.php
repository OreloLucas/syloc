<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ModelController extends Controller
{
    /* LISTAR */
    public static function listar($param, $header, $body)
    {
        $lista = ModelController::getTodos($param['clazz']);
        return ModelController::listarLista($lista, $param, $header, $body);
    }


    public static function listarLista($lista, $param, $header, $body)
    {
        $formName = $param['formName'];
        $formHeader = $param['formHeader'];
        $routes = $param['routes'];
        $excluir = $param['excluir'];
        $acoes = $param['acoes'];

        return view('model.listar',
            compact('lista', 'formName', 'header', 'formHeader', 'body', 'routes', 'excluir', 'acoes'));
    }
    /* LISTAR */


    /* ADICIONAR */
    public static function adicionar($param)
    {
        return ModelController::adicionarModel(null, $param);
    }

    public static function adicionarModel($model, $param)
    {
        $formName = $param['formName'];
        $form = $param['form'];
        $formHeader = $param['formHeader'];
        $routes = $param['routes'];
        return view('model.adicionar',
            compact('formName', 'model', 'form', 'formHeader', 'routes', 'script'));
    }
    /* ADICIONAR */


    /* EDITAR */
    public static function editar($id, $param, $alert = 'Cadastro não localizado', $alertClazz = 'alert alert-danger')
    {
        $model = ModelController::getUnico($param['clazz'], $id);
        return ModelController::editarModel($model, $param, $alert, $alertClazz);
    }

    public static function editarModel($model, $param, $alert = 'Cadastro não localizado', $alertClazz = 'alert alert-danger')
    {
        if (!$model || $model == null) {
            \Session::flash('mensagem', ['msg' => $alert, 'class' => $alertClazz]);
            return redirect()->route($param['routes']['lista']);
        } else {
            $formName = $param['formName'];
            $form = $param['form'];
            $formHeader = $param['formHeader'];
            $routes = $param['routes'];
            return view('model.editar',
                compact('model', 'formName', 'form', 'formHeader', 'routes'));
        }
    }
    /* EDITAR */


    /* DETALHE */
    public static function detalhe($id, $param, $alert = 'Cadastro não localizado', $alertClazz = 'alert alert-danger')
    {
        $model = ModelController::getUnico($param['clazz'], $id);
        return ModelController::detalheModel($model, $param, $alert, $alertClazz);
    }

    public static function detalheModel($model, $param, $alert = 'Cadastro não localizado', $alertClazz = 'alert alert-danger')
    {
        if (!$model || $model == null) {
            \Session::flash('mensagem', ['msg' => $alert, 'class' => $alertClazz]);
            return redirect()->route($param['routes']['lista']);
        } else {
            $formName = $param['formName'];
            $form = $param['form'];
            $formHeader = $param['formHeader'];
            $routes = $param['routes'];
            return view('model.detalhe',
                compact('model', 'formName', 'form', 'formHeader', 'routes'));
        }
    }
    /* DETALHE */


    /* SALVAR */
    public static function salvar($request, $param,
                                  $msgUpdate = 'Cadastro editado com sucesso', $msgCreate = 'Cadastro adicionado com sucesso',
                                  $clazzUpdate = 'alert alert-success', $clazzCreate = 'alert alert-success')
    {
        $model = $request->all();
        return ModelController::salvarModel($model, $param, $msgUpdate, $msgCreate, $clazzUpdate, $clazzCreate);
    }

    public static function salvarModel($model, $param,
                                       $msgUpdate = 'Cadastro editado com sucesso', $msgCreate = 'Cadastro criado com sucesso',
                                       $clazzUpdate = 'alert alert-success', $clazzCreate = 'alert alert-success')
    {
        try {
            if (isset($model['id'])) {
                ModelController::getUnico($param['clazz'], $model['id'])->update($model);
                \Session::flash('mensagem', ['msg' => $msgUpdate, 'class' => $clazzUpdate]);
            } else {
                $model['id_empresa'] = intval(auth()->user()['id_empresa']);
                $param['clazz']::create($model);
                \Session::flash('mensagem', ['msg' => $msgCreate, 'class' => $clazzCreate]);
            }
        } catch (\Exception $e) {
            \Session::flash('mensagem', ['msg' => 'Erro desconhecido ' . $e->getMessage(), 'class' => 'alert alert-danger']);
        }
        return redirect()->route($param['routes']['lista']);
    }


    public static function salvarModelAJAX($model, $param)
    {
        try {

            if (isset($model['id'])) {
                $model = ModelController::getUnico($param['clazz'], $model['id'])->update($model);
            } else {
                $model['id_empresa'] = intval(auth()->user()['id_empresa']);
                $model = $param['clazz']::create($model);
            }
            return $model;
        } catch (\Exception $e) {
            return ['error' => $e];
        }

    }

    public static function geraID($request, $param)
    {
        $model = $request->all();
        return ModelController::geraIDModel($model, $param);
    }

    public static function geraIDModel($model, $param)
    {
        try {
            if (isset($model['id'])) {
                return $model['id'];

            } else {
                $model['id_empresa'] = intval(auth()->user()['id_empresa']);
                $model = $param['clazz']::create($model);
                return $model['id'];
            }
        } catch (\Exception $e) {
            return ['success' => $e];
        }
    }
    /* SALVAR */


    /* DELETA */
    public static function deleta($id, $param,
                                  $msgDelete = 'Cadastro deletado com sucesso', $msgDeleterror = 'Cadastro não localizado',
                                  $clazzDelete = 'alert alert-success', $clazzDeleterror = 'alert alert-success')
    {
        $model = ModelController::getUnico($param['clazz'], $id);
        return ModelController::deletaModel($model, $param, $msgDelete, $msgDeleterror, $clazzDelete, $clazzDeleterror);
    }

    public static function deletaModel($model, $param,
                                       $msgDelete = 'Cadastro deletado com sucesso', $msgDeleterror = 'Cadastro não localizado',
                                       $classDelete = 'alert alert-success', $clazzDeleterror = 'alert alert-danger')
    {
        if (!$model || $model == null) {
            \Session::flash('mensagem', ['msg' => $msgDeleterror, 'class' => $clazzDeleterror]);
        } else {
            Storage::deleteDirectory(asset('storage/' . $model->imagem));
            //dd($model);
            $model->delete();
            \Session::flash('mensagem', ['msg' => $msgDelete, 'class' => $classDelete]);
        }
        return redirect()->route($param['routes']['lista']);

    }

    public static function deletaModelAJAX($model, //$param,
                                           $msgDelete = 'Cadastro deletado com sucesso', $msgDeleterror = 'Cadastro não localizado',
                                           $classDelete = 'alert alert-success', $clazzDeleterror = 'alert alert-danger')
    {
        try {
            if (!$model || $model == null) {
                \Session::flash('mensagem', ['msg' => $msgDeleterror, 'class' => $clazzDeleterror]);
            } else {
                Storage::deleteDirectory(asset('storage/' . $model->imagem));
                //dd($model);
                $model->delete();
//            \Session::flash('mensagem',['msg'=>$msgDelete,'class' => $classDelete]);
            }
            return ['success' => true];
        } catch (\Exception $e) {
            return ['success' => false];
        }

    }
    /* DELETA */


    /* UTILS */
    public static function getUnico($clazz, $id)
    {
        return $clazz::whereKey($id)->
        where([
            ['id_empresa', '=', auth()->user()['id_empresa']]
        ])->first();
    }

    public static function getLista($clazz, $id)
    {
        return $clazz::whereIn('id', $id)->
        where([['id_empresa', '=', auth()->user()['id_empresa']]])->get();
    }

    public static function getTodos($clazz)
    {
        return $clazz::where([['id_empresa', '=', auth()->user()['id_empresa']]])->get();
    }

    public static function callView($param)
    {
        $formName = $param['formName'];
        return view($param['route'],compact('formName'));
    }
    /* UTILS */
}
