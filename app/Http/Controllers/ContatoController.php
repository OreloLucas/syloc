<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\Contatos;
use App\Http\Requests\ContatoRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;


class ContatoController extends Controller
{
    private $param;

    public function __construct()
    {
        $this->param = [
            'formName' => "Contatos",
            'formHeader' => "contato",
            'form' => '_models.contato._form',
            'routes' => [
                "lista"     => "contatos",
                "novo"      => "contato.adicionar",
                "detalhe"   => "contato.detalhe",
                "editar"    => "contato.editar",
                "excluir"   => "contato.deleta",
                "deletaAJAX" => "/contato/deleta/",
                "salvar"    => "contato.salvar",
                "voltar"    => "contatos", ] ,
            'excluir' => "Deseja deletar o contato ",
            'clazz' => \App\Contatos::class,
            'acoes' => ['detalhe','editar','excluir'],
        ];
    }

    public function lista()
    {
        $param = $this->param;
        $header = [
            0 => "nome",
            1 => "fone1",
            2 => "email",
            3 => 'acoes',

        ];

        $body = [
            0 => "nome",
            1 => "fone1",
            2 => "email",
        ];

        $paginate = 10;
        return ModelController::listar($param,$header,$body);
    }

    public function adicionar(){
        $param = $this->param;
        return ModelController::adicionar($param);
    }

    public function editar($id = null)
    {
        $param = $this->param;
        $model = Contatos::find($id);

        if($model['clientes_id'] != 0)
            $model['cliente'] = Clientes::find($model['clientes_id'])['fantasia'];
        if($model['fornecedores_id'] != 0)
            $model['fornecedore'] = Clientes::find($model['fornecedores_id'])['fantasia'];

        return ModelController::editarModel($model,$param,'Contato não encontrado');
    }

    public function detalhe($id = null){
        $param = $this->param;
        return ModelController::detalhe($id,$param,'Contato não encontrado');
    }

    public function salvar(ContatoRequest $request){
        $param = $this->param;
        $request['id_referencia'] = 0;
        return ModelController::salvar($request,$param);
    }

    public function deleta(Request $request){//($id = null){
        //$param = $this->param;
        //return ModelController::deleta($id,$param);

        try {
            $id = $request->get('id');
            $model = Contatos::find($id);

            return ModelController::deletaModelAJAX($model);
//            return ['success' => true];
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }

//    public function vinculaCliente($id_cliente,$id_contato)
//    {
//        $model = Contatos::find($id_contato);
//        $model->clientes_id = $id_cliente;
//        $model->save();
//        return redirect()->route("cliente.editar",$id_cliente);
//    }
//
//    public function vinculaFornecedor($id_fornecedor,$id_contato)
//    {
//        $model = Contatos::find($id_contato);
//        $model->clientes_id = $id_cliente;
//        $model->save();
//        return redirect()->route("cliente.editar",$id_cliente);
//    }

    public function index(Request $request){
        $msg = $request->get('value');
        return ['success' => true, 'data' => $msg];
    }

}
