<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ControllerExemplo extends Controller
{
    private $param;

    public function __construct()
    {
        $this->param = [
            'formName' => "Produto",
            'formHeader' => "Produto",
            'form' => '_models.produto._form',
            'routes' => [
                "lista"     => "produto",
                "novo"      => "produto.adicionar",
                "detalhe"   => "produto.detalhe",
                "editar"    => "produto.editar",
                "excluir"   => "produto.deleta",
                "deletaAJAX" => "/produto/deleta/",
                "salvar"    => "produto.salvar",
                "voltar"    => "produto", ] ,
            'excluir' => "Deseja deletar o Produto",
            'clazz' => \App\Produtos::class,
            'acoes' => ['detalhe','editar','excluir'],
        ];
    }

    public function lista()
    {
        $param = $this->param;
        $header = [
            0 => "#",
            1 => "nome",
            2 => "endereco",
            3 => "numero",
            4 => 'acoes',
        ];

        $body = [
            0 => "id",
            1 => "nome",
            2 => "endereco",
            3 => "numero",
        ];
        $paginate = 10;
        return ModelController::listar($param,$header,$body);
    }

    public function adicionar(){
        $param = $this->param;
        return ModelController::adicionar($param);
    }

    public function editar($id = null){
        $param = $this->param;
        return ModelController::editar($id,$param,'Fornecedor não encontrado');
    }

    public function detalhe($id = null){
        $param = $this->param;
        return ModelController::detalhe($id,$param,'Fornecedor não encontrado');
    }

    public function salvar(LocalEntregaRequest $request){
        $param = $this->param;
        return ModelController::salvar($request,$param);
    }

    public function deleta(Request $request){
        // deleta trocando de view
        //($id = null){
        //$param = $this->param;
        //return ModelController::deleta($id,$param);


        try {
            $id = $request->get('id');
            $model = LocalEntregas::find($id);

            return ModelController::deletaModelAJAX($model);
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }
}
