<?php

namespace App\Http\Controllers;

use App\Fornecedores;
use App\Contatos;
use App\Http\Requests\FornecedorRequest;
use App\Http\Requests\ContatoRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\ModelController;


class FornecedorController extends Controller
{
    private $param;

    public function __construct()
    {
        $this->param = [
            'formName' => "Fornecedores",
            'formHeader' => "fornecedor",
            'form' => '_models.fornecedor._form',
            'routes' => [
                "lista"     => "fornecedores",
                "novo"      => "fornecedor.adicionar",
                "detalhe"   => "fornecedor.detalhe",
                "editar"    => "fornecedor.editar",
                "excluir"   => "fornecedor.deleta",
                "deletaAJAX" => "/fornecedor/deleta/",
                "salvar"    => "fornecedor.salvar",
                "voltar"    => "fornecedores", ] ,
            'excluir' => "Deseja deletar o fornecedor ",
            'clazz' => \App\Fornecedores::class,
            'acoes' => ['detalhe','editar','excluir'],
        ];
    }

    public function lista()
    {
        $param = $this->param;
        $header = [
//            0 => "#",
            1 => "razao",
            2 => "fantasia",
            3 => "cnpj/cpf",
            4 => 'acoes',
        ];

        $body = [
//            0 => 'id',
            1 => "razao",
            2 => "fantasia",
            3 => "cnpj|cpf",
        ];
        $paginate = 10;
        return ModelController::listar($param,$header,$body);
    }

    public function adicionar(){
        $param = $this->param;
        $result = ModelController::adicionar($param);
        //$result['this_contatos'] = $model->contatos;
        return $result;
    }

    public function editar($id = null){
        $param = $this->param;
        $model = Fornecedores::find($id);

        $result = ModelController::editarModel($model,$param,'Fornecedor não encontrado');
        $result['this_contatos'] = $model->contatos;
        return $result;
    }

    public function detalhe($id = null){
        $param = $this->param;
        $model = Fornecedores::find($id);

        $result = ModelController::detalheModel($model,$param,'Fornecedor não encontrado');
        $result['this_contatos'] = $model->contatos;
        return $result;
    }

    public function salvar(FornecedorRequest $request){
        $param = $this->param;
        $model = $request->all();

        if(!isset($model['is_pessoa_fisica'])){
            $model['is_pessoa_fisica'] = 0;
            $model['cpf'] = '';
        }else{
            $model['cnpj'] = '';
        }
        return ModelController::salvarModel($model,$param);
    }

    public function deleta(Request $request){//($id = null){
        //$param = $this->param;
        //return ModelController::deleta($id,$param);

        try {
            $id = $request->get('id');
            $model = Fornecedores::find($id);

            return ModelController::deletaModelAJAX($model);
//            return ['success' => true];
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }



    public function vinculaContato(ContatoRequest $request)//($id_cliente,$id_contato)
    {
        $param = $this->param;
        $param['clazz'] = \App\Contatos::class;
        return Contatos::vinculaContato($request,$param);
//        try {
//            $id = $request->get('id');
//            if ($id > 0) { //contato já existe
//                $contato = Contatos::find($id);
//                $contato->fornecedores_id = $request->get('fornecedores_id');
//                $contato->save();
//                return $contato;
//            } else { //contato não existe
//                $param = $this->param;
//                $param['clazz'] = \App\Contatos::class;
//                $model = Contatos::vincula($request);
//                return ModelController::salvarModelAJAX($model, $param);
//            }
//        } catch (\Exception $e) {
//            return ['success' => $e];
//        }
    }

    public function desvinculaContato(Request $request)//($id_contato)
    {
        $id_contato = $request->get('contato_id');
        try {
            $contato = Contatos::find($id_contato);
            $contato->fornecedores_id = 0;
            $contato->save();
            return ['success' => true];
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }

    public function gettotods(Request $request)//($id_contato)
    {
        $filtro = $request->get('filtro');
        try {
            return $this->getTodosContatos($filtro);
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }

    public function getTodosContatos($filtro = '')
    {
        $connection = config('database.default');

        $driver = config("database.connections.{$connection}.driver");

        if ($driver == 'mysql') {
            return Contatos::where([
                ['id_empresa', '=', auth()->user()['id_empresa']],
                ['nome', 'like', '' . $filtro . '%'],
                ['fornecedores_id', '=', 0]
            ])->orderBy('nome', 'asc')->get();
        } else if ($driver == 'pgsql') {
            return Contatos::where([
                ['id_empresa', '=', auth()->user()['id_empresa']],
                ['nome', 'ilike', '' . $filtro . '%'],
                ['fornecedores_id', '=', 0]
            ])->orderBy('nome', 'asc')->get();
        }
    }
}
