<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProdutoRequest;
use App\Produtos;
use Illuminate\Http\Request;
use App\Http\Controllers\ModelController;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\Exception;
USE Symfony\Component\HttpFoundation\File\MimeType;
use Illuminate\Http\UploadedFile;


class ProdutoController extends Controller
{

    private $param;

    public function __construct()
    {
        $this->param = [
            'formName' => "Produtos",
            'formHeader' => "produto",
            'form' => '_models.produto._form',
            'routes' => [
                "lista" => "produtos",
                "novo" => "produto.adicionar",
                "detalhe" => "produto.detalhe",
                "editar" => "produto.editar",
                "excluir" => "produto.deleta",
                "deletaAJAX" => "/produto/deleta/",
                "salvar" => "produto.salvar",
                "voltar" => "produtos",],
            'excluir' => "Deseja deletar o produto ",
            'clazz' => \App\Produtos::class,
            'acoes' => ['detalhe', 'editar', 'excluir'],
        ];
    }

    public function lista()
    {
        $param = $this->param;
        $header = [
//            0 => "#",
            1 => "cod",
            2 => "nome",
            3 => "valor locação",
            4 => 'acoes',
        ];

        $body = [
//            0 => "id",
            1 => "codigo",
            2 => "nome",
            3 => "preco_loca",
        ];

        $paginate = 10;
        return ModelController::listar($param, $header, $body);
    }

    public function adicionar()
    {
        $param = $this->param;
        return ModelController::adicionar($param);
    }

    public function editar($id = null)
    {
        $param = $this->param;
        return ModelController::editar($id, $param, 'Produto não encontrado');
    }

    public function detalhe($id = null)
    {
        $param = $this->param;
        return ModelController::detalhe($id, $param, 'Produto não encontrado');
    }

    public function salvar(ProdutoRequest $request)
    {
        $param = $this->param;
        $produto = $request->all();

        $produto['id'] = ModelController::geraIDModel($produto, $param);

        if (isset($produto['imagem']) && $produto['id'] != null) {
            $request->imagem->storeAs('public', auth()->user()['id_empresa'] . '/images/' . $produto['id'] . '/_1.' . $request->imagem->extension());
            $produto['imagem'] = '/' . auth()->user()['id_empresa'] . '/images/' . $produto['id'] . '/_1.' . $request->imagem->extension();
        }
        //dd($produto);
        return ModelController::salvarModel($produto, $param);
    }

    public function deleta(Request $request)
    {//($id = null){
        //$param = $this->param;
        //return ModelController::deleta($id,$param);

        try {
            $id = $request->get('id');
            $model = Produtos::find($id);

            return ModelController::deletaModelAJAX($model);
//            return ['success' => true];
        } catch (\Exception $e) {
            return ['success' => false];
        }
    }

    public function buscaProduto(Request $request)
    {
        $filtro = $request->get('filtro');
//        $filtro = '';
//        try {
            $connection = config('database.default');

            $driver = config("database.connections.{$connection}.driver");
            if ($driver == 'mysql') {
                return Produtos::where([
                    ['id_empresa', '=', auth()->user()['id_empresa']], // busca da mesma empresa
                    ['nome', 'like', '%' . $filtro . '%'] //busca baseado no filtro
                    //['clientes_id', '=', 0] //busca que não tem cliente
                ])->orderBy('nome', 'asc')->get();
            } else if ($driver == 'pgsql') {
                return Produtos::where([
                    ['id_empresa', '=', auth()->user()['id_empresa']], // busca da mesma empresa
                    ['nome', 'ilike', '%' . $filtro . '%'] //busca baseado no filtro
                    // ['clientes_id', '=', 0]//busca que não tem cliente
                ])->orderBy('nome', 'asc')->get();
            }
//        } catch (\Exception $e) {
//            return ['success' => false];
//        }
    }
}
