<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$this->get('id').'max:255',
            'password1' => 'in:'.$this->get('password2').'|min:6|max:20',
        ];
    }

    public function messages()
    {
       return [
           'name.required' => 'falta nome',
           'email.required' => 'falta email',
           'email.unique' => 'email não é unico',
           'password1.in' => 'senha diferente',
           'password1.min' => 'senha deve ter entre 6 e 20 caracteres',
           'password1.max' => 'senha deve ter entre 6 e 20 caracteres',
       ];
    }
}
