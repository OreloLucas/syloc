<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContatoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->ajax() && $this->get('id') <> 0 ){
            return [''];
        }else{
            return [
                'nome' => 'required',
                'cpf' => 'required',
            ];
        }

    }

    public function messages()
    {
        return [
            'nome.required' => 'falta nome',
            'cpf.required' => 'falta cpf',
        ];
    }
}
