<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $result = array();
        if($this->get('is_pessoa_fisica')){
            $result['cpf'] = 'required|max:14|min:14';
        }else{
            $result['cnpj'] = 'required|max:18|min:18';
        }

        $result['razao'] = 'required';
        $result['fantasia'] = 'required';
        $result['email'] = 'sometimes|nullable|email';
        $result['fone'] = 'max:16';
        return $result;
    }

    public function messages()
    {
        return [
            'cpf.required' => 'falta cpf',
            'cpf.min' => 'deve ter 14',
            'cpf.max' => 'deve ter 14',
            'cnpj.required' => 'falta cnpj',
            'cnpj.min' => 'deve ter 18',
            'cnpj.max' => 'deve ter  18',
            'razao.required' => 'falta razao',
            'fantasia.required' => 'falta fantasia',
            'fone.max' => 'tamanho maxmio 16',
            'email.email' => 'deve ser email',
        ];
    }
}
