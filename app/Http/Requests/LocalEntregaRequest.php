<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LocalEntregaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'endereco' => 'required',
            'numero' => 'required|numeric',
            //'bairro' => 'required',
            //'cidade' => 'required',
            //'UF' => 'required|min:2|max:2',
        ];
    }

    public function messages()
    {
        return [
            'codigo.required' => 'falta codigo',
            'nome.required' => 'falta nome',
            'preco_loca.required' => 'falta preco locacao',
            'preco_repo.required' => 'falta preco reposicao',

        ];
    }
}
