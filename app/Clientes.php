<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $fillable = [
        'id','id_empresa', 'razao', 'fantasia', 'cnpj', 'cpf' ,'ierg','fone','email', 'is_pessoa_fisica',
    ];

//    protected $dates = ['created_at','updated_at'];

    public function contatos() {
        return $this->hasMany('App\Contatos');
    }

    public function CountContatos() {
        return $this->hasMany('App\Contatos')->count();
    }
}
