<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $visible = [
        'cnpj', 'razao', 'fantasia', 'fone', 'email', 'endereco', 'numero', 'complemento',
        'bairro', 'municipio', 'UF','versao'
    ];


}
