<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model
{
    protected $fillable = [
        'id_empresa', 'fornecedores_id', 'codigo','nome','preco_loca', 'preco_repo', 'preco_custo' , 'imagem', 'descricao',
       // 'imageType','image'
    ];
}
